---
layout: page.liquid
title: Rapport du Shift Project sur la résilience du territoire
tags:
  - rapport
  - references
---

Les 3 tomes - Comprendre, Agir et Organiser - du rapport sont consultables sur le site du Shift Project à l'adresse: [theshiftproject.org/article/manuel-resilience-elus-et-collectivites/](https://theshiftproject.org/article/manuel-resilience-elus-et-collectivites/)

Le Shift Project a fait le choix de s'adresser, en priorité, aux élus et techniciens des collectivités territoriales mais aussi à tous les autres acteurs de la résilience des territoires (entrepreneurs, responsables associatifs, consultants, agents de l’Etat, citoyens).

## 2 objectifs clés

+ Interpeller les acteurs territoriaux sur les conséquences des bouleversements écologiques en cours
+ Leur fournir des outils pour construire la résilience de leur territoire face aux chocs à venir
