---
layout: page.liquid
date: 2022-05-15

title: Les dimensions possibles du territoire
tags:
  - article
  - cartographie
  - mobilite
  - logistique

featured: "home"


---

<p class="chapo">
Quel pourrait être la taille critique d'un territoire pour penser une mise en adéquation de ses ressources et des besoins fondamentaux de ses habitants ?
</p>

<!--more-->

## L'Aven, un pays de traditions

<figure>
  <img src="bretagne-pays-historiques-640-16.png" alt="Les pays historiques de la Cornouaille.">
  <figcaption>Les pays de traditions de la Cornouaille dont les limites varient selon les expressions artistiques - danse, musique, habillement...</figcaption>
</figure>

&rarr; Voir [la carte de toute la Bretagne](https://www.geobreizh.bzh/wp-content/uploads/2020/06/2020-04_Poster_Bretagne-pays-historiques_Multinatio.jpg) sur le site [geobreizh.bzh/](https://www.geobreizh.bzh/).

## le découpage en départements

En 1790, la taille des départements avait été fixée de façon telle qu'il devait être possible de se rendre en moins d'une journée de cheval au chef-lieu de chacun de ceux-ci depuis n'importe quel point de leur territoire (Voir Matthieu Crozet et Miren Lafourcade, La nouvelle économie géographique, La Découverte, 2010, p. 40.).

## Logistique et mobilité

Logistique et mobilité (cf. Défi A de l'ADEME) semblent clés pour déterminer la taille critique d'un territoire dans le cadre de la transition écologique.

La fabrique des mobilités s'interroge sur une <a href="https://lafabriquedesmobilites.fr/blog/mobilite-minimum-viable">moblité minimum viable</a>. <q>La capacité d’un territoire à pouvoir vivre correctement en appelant un minimum de Mobilité devient une qualité <q>évidente</q>. Malheureusement, cela n’a aucun rapport avec tous les indicateurs dont nous disposons. De même pouvoir déplacer des marchandises essentielles avec le minimum de moyens, de ressources, avec des modes de transport eux-mêmes les plus résilients vers les personnes les plus fragiles et dépendantes. Ces deux indicateurs renvoient immédiatement à l’urbanisme et l’organisation du territoire (densité, mixité, production alimentaire proche des habitants, etc), mais également à la cohésion sociale et sa capacité à agir collectivement.</q>

Lire [l-extreme-defi-de-l-ademe/](/articles/l-extreme-defi-de-l-ademe/)

## La campagne du 1/4 d'heure

Cette idée fait référence au concept de "ville du quart d'heure" - une proposition de développement d'une ville polycentrique, où la vie en proximité assure une mixité fonctionnelle en développant les interactions sociales, économiques et culturelles. Ce concept est basé sur un modèle ontologique de la ville pour répondre aux besoins de chacun à partir de 6 catégories de fonctions sociales :

+ habiter
+ travailler
+ s'approvisionner
+ se soigner
+ s'éduquer
+ s'épanouir

 Voir [Ville du 1/4 d'heure](https://fr.wikipedia.org/wiki/Ville_du_quart_d%27heure)
 Lire [Territoiree de la 1/2 heure](https://www.lemonde.fr/idees/article/2024/04/07/carlos-moreno-la-france-peut-veritablement-se-diriger-vers-la-concretisation-des-territoires-de-la-demi-heure_6226521_3232.html)
