---
layout: page.liquid
date: 2022-04-15
title: Graphisme et territoire
tags:
  - acteurs
  - lieux
  - graphisme
  - édition

featured: "home"
---

<p class="chapo">Le graphisme est indispensable à la production et à la diffusion de récits sur le territoire.</p>

<!--more-->

## Références finistèriennes à classer

+ [Super Banco](https://super-banco.com/)
+ [Atelier téméraire](https://atelier-temeraire.tumblr.com/)


## Autres références à classer

+ [Terrains vagues](https://terrainsvagues.fr) est un collectif de graphisme fondé en 2014 par Maria-del-sol Abeilhe Godard, Ambre Langlois et Elsa Varin. Nous travaillons actuellement dans un atelier au sein de Garage Coop, dans le quartier du Port du Rhin à Strasbourg. Nous concevons des projets de communication, d’édition, de mise en espace et de médiation. Nous réalisons aussi des ateliers participatifs.
