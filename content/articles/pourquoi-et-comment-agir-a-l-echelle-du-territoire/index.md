---
layout: page.liquid
title: Pourquoi et comment agir à l'échelle du territoire ?
tags:
  - action
---

Cette page fait suite à une discussion initiée pendant une des collégiales de l'association [Bretagne Transition](/articles/les-enjeux-de-bretagne-transition/) et reprend des idées échangées au sein de notre groupe.

<!--more-->

## Territoire et besoins élémentaires

La dimensionnement de l'espace de déploiement des projets et actions pour la sauvegarde de cet espace doit être envisagé en accord avec une vision de neutralité carbone à horizon 2050. Si cet objectif est aujourd'hui purement théorique (et ne semble être une priorité ni pour l'État français ni pour les sociétés du CAC40) il permet, à l'instar [des derniers scénarios de l'Ademe](https://transitions2050.ademe.fr/), de se positionner entre une utopie politique de la sobriété (ou utopie terrienne) et une utopie technologique solutionniste (ou utopie hors-sol).

Si en 1790, la taille des départements avait été fixée de façon telle qu'il devait être possible de se rendre en moins d'une journée de cheval au chef-lieu de chacun de ceux-ci depuis n'importe quel point de leur territoire, il est envisageable de penser que la grande majorité des échanges entre individus du territoire ne se déployaient que très rarement à l'échelle du département et que le déplacement au chef-lieu était rare (<mark>chiffres?</mark>).

La conjonction des besoins élémentaires des individus et des nouvelles contraintes à déployer pour la sauvegarde de la biosphère doit permettre d'avoir une idée plus précise de la dimension de cet espace de vie. Si ces besoins sont avant tout la meilleure santé possible, de quoi manger, se loger, travailler, le tout dans un cadre social assurant les points précédents (selon le besoin de subsistance de Max-Neef), les leviers d'actions pour la neutralité carbone seraient donc, dans un premier temps, la santé, l'alimentation, l'habitat et l'énergie, la mobilité et l'emploi, le tout dans un cadre social encourageant l'action et la mesure de ces leviers.

Quel pourrait être la taille critique d'un territoire pour penser une mise en adéquation de ses ressources et des besoins fondamentaux de ses habitants ?

## Pourquoi agir à l'échelle du territoire ?

Point Bookchin et écologie sociale

Pour certains individus contraint à toutes les dimensions de la précarité, la taille du territoire peut être déjà trop grand  (Par exemple il peut paraître difficile pour un habitant non motorisé de Nevez d'envisager travailler à Rosporden)

La mobilité

Logement/transport/travail - les jeunes.

Connaissance des acteurs

## Comment agir à l'échelle du territoire ?

+ Rassembler toutes les personnes du territoire intéressées par le besoin de transformation pour des objectifs sociaux ou environnementaux.

Jouer un rôle de médiation entre les acteurs (forces vives) du territoire et avec l'ensemble de ces acteurs et les résidents du territoire.


Quelques notes avant de faire un article complet.

+ Difficultés à saisir les enjeux environnementaux - échelle trop globale
+ Ou des enjeux environnementaux vu comme non prioritaires
+ Difficultés à manier chiffres et concept
+ Comment jouer avec les élus et les instances publiques


## Solutions

+ Faire participer la population à la mesure
+ La méthode scientifique
+


## Utopie technologique vs. utopie politique (ou sociale)
