---
layout: page.liquid
title: Les thématiques de la résilience
tags:
  - article
  - donnees
---

<p class="chapo">
Il est difficile d'établir une taxonomie intelligible qui permet de couvrir l'ensemble des thématiques touchant aux concepts de résilience et de transition d'un territoire. Les exemples suivants montrent la diversité des approches.
</p>

<!--more-->

## Les dynamiques de Transiscope

Aujourd’hui, de nombreux d’acteurs de la transition et des alternatives ont entamé un travail de recensement et de cartographie de leurs organisations, actions et écosystèmes. Pour permettre de relier ces alternatives, une dizaine de collectifs travaillent depuis deux ans pour développer des outils libres permettant de connecter les différentes bases de données existantes et de les visualiser au même endroit:
[Transiscope](https://transiscope.org/carte-des-alternatives/#/carte/bretagne@47.96,-3.81,10z?cat=all)

+ Écologie, Alimentation, Énergie
  + Agriculture et alimentation
  + Climat et énergie
  + Eau, nature et biodiversité
+ Culture, Médias et Lien social
  + Culture
  + Espaces de rencontres et de lien social
  + Médias et numérique
+ Bien-être physique et personnel
	+ Santé
	+ Personnes âgées
	+ Handicap
	+ Développement personnel, bien-être
+ Citoyenneté, droits, solidarités
	+ Solidarité
	+ Citoyenneté, droits, inclusion
	+ Intelligence collective
+ Éducation, formation
	+ Education
	+ Sensibilisation
	+ Formation
+ Habitat et urbanisme
	+ Habitat, logement
	+ Aménagements et projets urbains
+ Modes de déplacements
+ Économie Sociale et Solidaire
	+ Fabriquer, réparer, zéro déchets
	+ Systèmes d'échange alternatifs
	+ Finance éthique
	+ Producteurs, fournisseurs
	+ Magasins, commerces

## Les thématiques du Shift Project

Dans son rapport *La résilience des territoires pour tenir le cap de la transition écologique* le Shift Project propose cinq portes d’entrée opérationnelles vers la résilience et la transition écologique de son territoire, cinq domaines, transverses et systémiques, pour engager le territoire:
+ l’administration locale,
+ l’alimentation,
+ l’urbanisme & l’aménagement,
+ l’économie & l’emploi,
+ la santé & le bien-être.

## Les défis de l'ADEME

Issue du travail interne ADEME, [la résilience des territoires](https://resilience-territoire.ademe.fr/) est définie comme un territoire en mouvement, capable :

+ d’anticiper des perturbations, brutales ou lentes ; la veille et à la prospective sont clés ;
+ d’en atténuer ou absorber les effets ;
+ de se relever et de rebondir grâce à l’apprentissage, l’adaptation et l’innovation ;
+ d’évoluer vers un nouvel état en « équilibre dynamique » préservant ses fonctionnalités. Cet état devrait être décidé et construit collectivement.

Dans ce cadre 9 défis ont été définis

+ Défi 1 - Connaissances, Ressources, Projets (indexer, ordonner, pouvoir manipuler)
+ Défi 2 - Objectifs et Diagnostic d'un territoire
+ Défi 3 - Mieux décider et agir ensemble
+ Défi 4 - Comptabilité et Monnaie de la résilience
+ Défi A - Logistique et Mobilité
+ Défi B - Alimentation et agriculture
+ Défi C - Urbanisme Circulaire
+ Défi D - Ilots de chaleur
+ Défi E - Gestion de l'Eau et des sols

## La boussole de la résilience du Cerema

La boussole de la résilience constitue une première tentative de synthèse et de formalisation des enseignements théoriques et pratiques collectés et une première ébauche vers un « référentiel » de la résilience. La boussole propose un cadre d’action pour les collectivités, organisé en six principes, déclinés en dix-huit leviers.

+ Stratégies
  + Gouvernance
  + Gouvernance partagée
  + Coopération territoriale
  + Intégration des vulnérabilités
+ Cohésion
  + Solidarité
  + Culture et savoir faire - Solidarité
  + Capacité d’agir
+ Anticipation
  + Veille
  + Connaissance des menaces - Information - éducation
  + Préparation à la crise
+ Adaptation, apprentissage et innovation
  + Surveiller et alerter
  + Rex et benchmarks
  + Innovation, expérimentation
+ Sobriété et besoins essentiels
  + Besoins vitaux et essentiels
  + économie résiliente
  + Limites planétaires et ressources
+ Robustesse et continuité
  + Exposition réduite
  + Solidité et fiabilité
  + Continuité des services

---

## Lire aussi

+ Un [projet d'arpentage](/articles/arpenter-le-territoire/) va être initier afin d'explorer le paysage pour et grace à la production de données et d'analyser le rapport étroit entre activités et paysage.
+ [L'ontologie du pays de l'Aven](/articles/l-ontologie-du-pays-de-l'aven/)
+ [Des données interopérables](/articles/des-donnees-interoperables/)
