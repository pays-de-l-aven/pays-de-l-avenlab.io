---
layout: page.liquid
date: 2022-05-15
title: Un maillage convivial
tags:
  - mobilite
  - logistique

featured: "home"

---

<p class="chapo">Le concept de la ville du 1/4 d'heure propose à tout habitant, dans un rayon de 15 minutes à pied de son lieu d'habitation, la possibilité de travailler, de s'approvisionner, de se soigner, de s'éduquer et de s'épanouir. Si ce concept est envisageable dans une mégapole hyper dense, la proposition offerte n'est possible ni dans les espaces periurbains ni dans les espaces ruraux.</p>

<!--more-->


L'illustration ci-dessous présente 2 trames à l'échelle du territoire de la CCA. Les grand cercles orange sont espacés de 6 kms pour que chaque habitant du territoire puisse avoir accès à un de ces points en moins d'1/4 heure à vélo. Les petits cercles orange sont espacés de 2 kms pour que chaque habitant du territoire puisse avoir accès à un de ces points en moins d'1/4 heure à pied.

<mark>Pour exemple, répertorier la localisation des boulangeries sur le territoire et la positionner en regard de la trame piéton.</mark>

En traçant un cercle d'un rayon égal à la distance parcourue en 1/4 d'heure à vélo à partir de chaque centre ville, des zones blanches apparaissent : entre Tourch et Rosporden, entre St-Yvi, Melgven et Concarneau et entre Melgven et Pont-Aven.

<figure>
  <img src="/articles/un-maillage-convivial/territoire-du-quart-d-heure-pieton-velo-001.png" alt="Un maillage convivial pour les piétons et vélocipédistes">
  <figcaption>Un maillage pour les piétons et vélocipédistes pas encore assez convivial.</figcaption>
</figure>
