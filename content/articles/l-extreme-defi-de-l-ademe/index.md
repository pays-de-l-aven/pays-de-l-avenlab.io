---
layout: page.liquid
date: 2022-03-15

title: L'Extrème Défi de l'Ademe
tags:
  - mobilite
  - territoire

featured: "home"

---

<p class="chapo">Comment initier une démarche ouverte et innovante pour concevoir les véhicules de demain? Comment faire 1000x mieux que la voiture au quotidien par une collection d'objets roulants véhiculant 1 à 2 personnes et une charge de 100kg ou bien 3 personnes et leurs sacs? L'extreme défi de l'ADEME est un concours/challenge en 3 ans de 2021 à 2023 véhicules intermédiaires transition écologique systèmes de mobilité industries nouvelles entre le vélo et la voiture. Ci-dessous un début de réflexion à l'échelle de la CCA.</p>

<!--more-->

## Penser l’objet roulant au delà de l’objet roulant

En 2014, pour répondre à ces différents besoins à l'échelle de la CCA, 3% des actifs utilisaient les transports en commun (offre peu développée et peu pratique) contre 86% qui utilisaient voiture, camion ou fourgonnette (source Évaluation à 6 ans du Scot de la CCA). Toujours en 2014, 2% utilisaient un 2 roues et 5% la marche à pied. Et pourtant selon les recensements annuels, 42% des personnes dont le lieu d’emploi est situé à moins d’un kilomètre de chez eux prennent le plus souvent leur voiture pour s’y rendre (*chiffre à vérifier pour la CCA*).

*Je n'ai pas trouvé des chiffres plus récents mais cela ne doit pas être très différent.*

l'Extreme Défi consiste bien à faire diminuer de manière drastique ce monopole et de la voiture, du camion ou de la fourgonnette. Mais peut-on penser simplement remplacer des SUVs par des voiturettes pour atteindre la neutralité carbone en 2050 ? À la lecture du Transition(s)2050 de l'Ademe, le Scénario 1 - Génération frugale - envisage une baisse de la demande de mobilité importante grâce à d'avantage de proximité, qui favorise les modes actifs : marche, vélo... ainsi qu'un développement du covoiturage solidaire et de l'autostop dans les zones rurales.

Limiter les routes à 30 km/h pourrait avoir plus d'impact sur ce monopole que n'importe quel nouveau type de véhicule. IL s'agit donc ici non pas de remplacer le modèle voiture, camion ou fourgonnette par un nouveau modèle mais de transformer l'ensemble de l'écosystème de la voiture - routes, station services, garages, compagnies pétrolières, familles nucléaires, l'habitat pavillonnaire ou individuel, zones d'activités...

## Une approche systémique pour penser une alternative à la voiture

L'approche design devrait être plus systémique et envisager les dépendances fortes de la mobilité avec les besoins humains élémentaires à l'échelle du territoire que sont l'alimentation, le logement, le travail, la formation, les loisirs, l'indépendance...

(C'est bien l'encouragement du modèle de la famille nucléaire qui a permis le développement de la voiture et non l'inverse.)

Nous avons vu l'impact du télétravail sur les mobilités pendant la crise sanitaire (qui ne va pas toujours dans le bon sens - cf. souffrance des femmes pendant cette période — *chiffres à chercher pour la CCA*) qui nous fait penser que les actions ne doivent uniquement se concentrer sur la conception et la fabrication de "nouvelles voitures" (comme ce qu'on pu proposer les constructeurs/greeenwashers automobiles avec la voiture électrique).

## Le territoire du 1/4 d'heure - un maillage convivial

Cette idée fait référence au concept de "ville du quart d'heure" - une proposition de développement d'une ville polycentrique, où la vie en proximité assure une mixité fonctionnelle en développant les interactions sociales, économiques et culturelles. Ce concept est basé sur un modèle ontologique de la ville pour répondre aux besoins de chacun à partir de 6 catégories de fonctions sociales :

+ habiter
+ travailler
+ s'approvisionner
+ se soigner
+ s'éduquer
+ s'épanouir

L'illustration ci-dessous présente 2 trames à l'échelle du territoire de la CCA. Les grand ronds orange sont espacés de 6 kms pour que chaque habitant du territoire puisse avoir accès à un de ces points en moins d'1/4 heure à vélo. Les petits ronds orange sont espacés de 2 kms pour que chaque habitant du territoire puisse avoir accès à un de ces points en moins d'1/4 heure à pied (cela reste une approximation à vol d'oiseau).

*Pour exemple, répertorier la localisation des boulangeries sur le territoire et la positionner en regard de la trame piéton. Idem pour le logement, l'activité économique...*

Ce maillage peut-être l'infrastructure clé d'une nouvelle mobilité, comme par exemple la création d'un atelier de réparation à chaque point de la trame piéton. À partir de cette simple idée, imaginons une nouvelle version des [relais de poste](https://fr.wikipedia.org/wiki/Relais_de_poste).

<figure>
  <img src="/articles/un-maillage-convivial/territoire-du-quart-d-heure-pieton-velo-001.png" alt="Un maillage convivial pour les piétons et vélocipédistes">
  <figcaption>Un maillage pour les piétons et vélocipédistes pas encore assez convivial.</figcaption>
</figure>

En traçant un cercle d'un rayon égal à la distance parcourue en 1/4 d'heure à vélo à partir de chaque centre ville, des zones blanches apparaissent : entre Tourch et Rosporden, entre St-Yvi, Melgven et Concarneau et entre Melgven et Pont-Aven.

## Des relais de mobilités

Ces relais pourraient, quotidiennement, mettre à disposition d'un marcheur et d'un vélocopédiste les denrées et services nécessaires (ou au moins une grande majorité).

Des relais qui pourraient être :

+ Des bistrots - on est en Bretagne ou bien ?
+ Des espaces d'information et de socialisation (voire d'inclusion)
+ Des dépôts de denrées alimentaires
+ Des ateliers de réparation - mobilité, outillage... - et de réemploi
+ Des espaces de location de matériels
+ Des espaces de co-working et de formations
+ ...

## Des véhicules pour la logistique... et le transport en commun.

Ce scénario de maillage convivial met les moyens logistiques au cœur des besoins de transformation. Il s'agit, dans un premier temps, de penser des alternatives au camion et à la fourgonnette et non plus à la voiture dont la perte d'hégémonie en sera la conséquence. Voir le projet [La Remorque Partagée](https://bretagnetransition.mystrikingly.com/blog/la-remorque-partagee) utilisée aujourd'hui par [Belo Livraison](https://belolivraison.fr/).

Imaginons des scénarios à contrainte forte comme l'interdiction de mobilité personnelle motorisée si elle n'est pas accompagné d'une prise en charge d'une partie de ce besoin logistique. Et inversement on peut penser un réseau logistique durable qui offre en complément une possibilité de transport de personnes.

à suivre

---

## Annexes

### Programme d'actions Cit'ergie CCA 2020-2022

Pour rappel, les actions "mobilité" pour la période 2020-2022 sont les suivantes :

+ Définir les modalités de suivi des parts de chaque mode de déplacement (EDVM ou google)
+ Agir sur les changements de comportement liés aux mobilités.
+ Poursuivre les actions de développement de la multimodalité et la promotion du covoiturage.
+ Poursuivre et amplifier les actions de sensibilisation des agents à la mobilité durable.
+ Développer les formations d'écoconduite.
+ Engager une réflexion avec les professionnels de la logistique pour étudier les possibilités d'action.
+ Poursuivre et développer l'accompagnement des communes pour le développement du réseau piétonnier du quotidien.
+ Poursuivre et développer l'accompagnement des communes pour le développement du réseau cyclable du quotidien.
+ Suivre le déploiement des infrastructures cyclables.
+ Aménager le pôle d'échange multimodal de Rosporden.

### Liens divers

+ [Extrême Défi de l'Ademe](https://xd.ademe.fr/)
+ [L'Extreme Défi sur le wiki de la FabMob](https://wiki.lafabriquedesmobilites.fr/wiki/L%27extr%C3%AAme_d%C3%A9fi_ADEME)
+ [Lien équipe Californouaille](https://wiki.lafabriquedesmobilites.fr/wiki/Equipe_XD_Californouaille)

---
(un point sur le mail de Jeff [monorail_dans_le_vent](https://bricoles.du-libre.org/bricoles:monorail_dans_le_vent))

Salut la compagnie
L'appel à projet me semble déjà un peu biaisé , on y parle de mobilité
mais pour causer de mobile, je trouve que le mobile est un peu faible ;)
le topo de PYS est super, tu as la même réaction que moi sur le sujet.
avant de chercher des énième solutions comme un troupeau d'ingénieur
enfermés dans un bureau sans café, je me dis comme PYC que peut etre il
faut prendre du recul.
Déjà mobilité , c'est pourquoi, pour qui , deja en réduire la nécessité
plutot que la facilité , ca sert a quoi d'avoir une voiture si on en pas
besoin , un velo si on ne sait ou ne peut pas en faire. Il faut inclure
toute la population dans l'histoire, les enfants, les ado, les mères et
pères, ceux qui on des activités, les vieux, etc... pourquoi ont ils
besoin de bouger, dans quel mesure, comment minimise t on cela?
je trouve que les plus belles voitures sont celle de César.
PYC à bien exposé le début.
Perso je ne m’intéresse pas à concevoir un mobile industrialisable, même
si c'est rigolo de s'en faire un , il ne sera qu'une oeuvre artistique,
pertinentes mais non reproductible en tant que tel , ce qui est
reproductible c'est le chemin de la réflexion, les gestes, le savoir, la
politique. Ainsi ce qui est important c'est de savoir mettre en place
des espaces temps qui permettent aux habitants des territoires dans ce
soucis de résilience, de prendre en main eux même le savoir, la
réalisation, l'évolution de leurs outils et objets de mobilité.
pas d'acheté et d'être ainsi dépossédé, simple consommateur, de bidule
mobile.
Les questions à répondre, à qui sont les routes, les chemins ? à qui
sont les temps de déplacements, les informations liées autant que les
outils. Ne peut on pas les penser en tant que Commun ?
Ensuite comment participe t on au décision impliquant les citoyens, y a
t-il coopération/tives collétif/vité, est Libre ? L'usage d'un simple
vélo implique aussi l'usage de la route, du temps, l'implication des
autres, le partage , l'usage de code et l'acceptation de conventions,
qui,comment,quand se synchronise t on ?
La route n'est plus l'outil idéal de déplacement, (le train non plus)
,parce que la voiture doit être repensée, il faut donc repenser la
route.
repenser les routes, le réseau routier.
et apres on pourra parler de ce qu'on met dessus

Cela me rappelle l'idée des mini-trains, des monorails , je vous fait un
topo par la
https://bricoles.du-libre.org/bricoles:monorail_dans_le_vent


---

Lire aussi: [Un schéma directeur mobilité](/articles/un-schema-directeur-mobilite/)
