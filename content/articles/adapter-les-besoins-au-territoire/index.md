---
layout: page.liquid
date: 2021-12-01
title: Adapter les besoins au territoire
tags:
  - acteurs
  - lieux
  - donnees

---

Quelques ressources.

<!--more-->

Lire *[De l’ajustement à la transformation : vers un essor de l’adaptation ?](https://journals.openedition.org/developpementdurable/11320)* de Guillaume Simonet.

<blockquote cite="https://journals.openedition.org/developpementdurable/11320">En appelant les activités humaines à s’adapter aux territoires dans une logique dynamique, l’adaptation transformationnelle convie à une remise en question des objectifs de l’action publique en place, des trajectoires de développement envisagées voire du rapport entretenu avec la nature. [...] Inévitablement se pose la question d’une « capacité de charge », amenant à s’interroger sur le nombre (démographie) et le niveau de vie souhaitable (et souhaité), autant de questions relativement absentes des réflexions et des discours aussi bien dans le monde scientifique, social que politique.</blockquote>

### youhou

## Définir la "capacité de charge"

Tout l'enjeu consiste à ajuster ressources critiques et bien-être critique. <mark>à développer</mark>

&rarr; Lire [Identifier le Capital Naturel Critique](/articles/identifier-le-capital-naturel-critique/)

## Ressources

+ Lire la thèse de Pelenc *[Développement humain responsable et aménagement du territoire](https://mpra.ub.uni-muenchen.de/56094/1/MPRA_paper_56094.pdf)*
+ Chercher Liens entre services écosystémiques et différentes dimensions du bien-être d'après l’évaluation des écosystèmes du millénaire
+ Chercher L’approche de TEEB (The Economics of Ecosystems and Biodiversity, 2010)
+ Faire une lecture critique de [la théorie du Donut de Kate Raworth](https://fr.wikipedia.org/wiki/Doughnut_(mod%C3%A8le_%C3%A9conomique))
