---
layout: page.liquid
title: Des données interopérables
tags:
  - donnees
---

<p class="chapo">Les données des acteurs et actions en faveur de la transformation du territoire doivent non seulement être produites mais également mutualisables.</p>

<!--more-->

Le [projet SITI](https://wiki.resilience-territoire.ademe.fr/wiki/SITI) qui va dans ce sens de l'interopérabilité est très bien documenté sur le wiki *Résilience des territoires* de l'Ademe. Il propose de mettre en œuvre une démarche agile et coopérative, mobilisant des solutions logicielles ouvertes et interopérables, pour co-construire des systèmes d’information en biens communs au service de la résilience des territoires.

Voir aussi [dataterra.fr/](https://dataterra.fr/)

Pour comprendre l'enjeu de la documentation et de la production de données structurées et interopérables, lire l'article des Chemins de la transition sur [les technologies utilisées](https://lescheminsdelatransition.org/les-technologies-utilisees-2/).

Découvrir ou approfondir le projet semapps de l'Assemblée Virtuelle [virtual-assembly.org/semapps/](https://www.virtual-assembly.org/semapps/). Voir aussi [semapps.org/](https://semapps.org/) et la [formation semapps](https://github.com/assemblee-virtuelle/semapps/wiki/Formation-SemApps-%28en-fran%C3%A7ais%29)

Découvrir ou approfondir le projet Solid [solidproject.org/](https://solidproject.org/). Lire aussi [Un avenir Solid](https://blog.orgtech.fr/un-avenir-solid/).

## Définition de l'ontologie des données

L’[ontologie PAIR](https://www.virtual-assembly.org/ontologie-pair/) est issue d’un travail collectif mené par l’association Assemblée virtuelle et ses partenaires. Il s’agit d’une part, de fournir un ensemble de concepts et de relations génériques permettant décrire la diversité des communautés:

### Liste des concepts

+ Personnes
+ Organisations
+ Propositions
+ Projets
+ Thèmes
+ Biens
+ Services
+ Compétences
+ Événements
+ Lieux
+ Dates
+ Documents

### Exemples de relations entre ces concepts

+ [Personne] est membre de [Organisation]
+ [Organisation] est impliquée dans [Projet]
+ [Projet] a pour thématique [Thème]
+ [Thème] est un centre d’intérêt de [Personne]...

L’ontologie PAIR permet ainsi de structurer des réseaux sociaux communautaires.

Il s’agit d’autre part de fournir un modèle basé sur les standards du web sémantique, ceci afin que différentes communautés puissent se relier par l’entremise de systèmes d’informations interopérables.

## lire

+ [15 strategies ministerielles et 500 actions pour accelerer](https://www.etalab.gouv.fr/politique-de-la-donnee-des-algorithmes-et-des-codes-sources-15-strategies-ministerielles-et-500-actions-pour-accelerer)
+ [Pour un féminisme des données](https://www.internetactu.net/2021/09/27/pour-un-feminisme-des-donnees/)


## Lire aussi

+ [Les thématiques de la résilience](/articles/les-thematiques-de-la-resilience/)
+ Un [projet d'arpentage](/articles/arpenter-le-territoire/) va être initier afin d'explorer le paysage pour et grace à la production de données et d'analyser le rapport étroit entre activités et paysage.
+ [L'ontologie du pays de l'Aven](/articles/l-ontologie-du-pays-de-l'aven/)
