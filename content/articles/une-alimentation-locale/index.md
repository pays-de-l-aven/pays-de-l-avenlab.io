---
layout: page.liquid
title: Une alimentation locale
tags:
  - alimentation
---

<p class="chapo">Bien se nourrir grâce à une alimentation produite localement.<p>

<!--more-->

## Lundi

+ Marché à Concarneau Matin (Hebdomadaire), Place Jaurès.

## Mardi

+ Marché à Pont-Aven Matin (Hebdomadaire), Place de l'Hôtel de Ville.
+ Distribution Amap de Concarneau le mardi de 18h30 à 19h30 à l'abri du marin - 19, Place Duquesne.


## Mercredi

+ Marché à Trégunc Matin (Hebdomadaire), Place de la Mairie.
+ Pain du Facteur Mouton à la librairie Albertine à Concarneau
+ Distribution Amap Riec-sur-Belon de 18h15 à 19h30 à la salle Ty-Forn - 4, rue du presbytère. Riec-sur-Belon

## Jeudi

+ Recevez votre livraison de légumes ramassés la veille à la petite ferme de Kerkaudan grâce à [Belo Livraison](https://belo.coopcycle.org/fr/shops?type=grocery-store)
+ Marché à Rosporden Matin (Hebdomadaire), Places.

## Vendredi

+ Marché à Concarneau Matin (Hebdomadaire)
  + Pain du Facteur Mouton
  + Nicolas Picollec - maraicher Bannalec

## Samedi

+ Caisse de viande de porc Terre et Paille entre 9h et 13h (commande préalable) - La ferme de Bossulan — Terre et Paille, 29930 Pont-Aven (caissette de 8/9kg)

## Dimanche

+ Marché à Trégunc Matin (Saisonnier), Trevignon.
+ Marché à Trégunc Toute la journée (Saisonnier), Place de la Mairie.

---
## Amap

### Amap de Concarneau

4 ave du Dr Nicolas 29900 Concarneau
Produits : Légumes, oeufs, produits laitiers, fruits rouges de saison, jus de pomme et cidre
Distribution le mardi de 18h30 à 19h30 à l'abri du marin - 19, Place Duquesne.
Contact Gwen Godefroy Tél. : 06 17 19 16 90

### La main au panier à moelan-sur-mer

( copier et enlever les espaces : lamainaupanier @ laposte.net)
Produits : Légumes BIO
Distribution le vendredi de 16h à 18h à la Ferme -
Kernijeanne Coz.

### Amap de riec à Riec-sur-Belon

amap.riec @ gmail.com
Les producteurs de l’Amap viennent livrer leurs produits le mercredi soir de 18h15 à 19h30 à la salle Ty Forn (4 rue du presbytère) à Riec-sur-Belon. Producteurs: Robin Silvent – légumes - Les jardins bio du Belon – légumes - La ferme de Kerségalou – produits laitiers - Bara’laezh – pains - Breizh pommes – pommes et jus de pommes - Saveurs en chemin – gelée de fleurs, pesto - Ferme de Kimerc’h – fromage de vache - Baume Shanti – produits de soin à base de plantes médicinales - Laura et ses chèvres – fromage de chèvre - Olivier Blondin – miel - Bio Pizza – pizzas - La poule mouillée – poulets bio - Peau de vache – savons - Ty Jaouen – Bières Artisanales - Farfad’algues – tartares d’algues - Bruno Thomas – viande d’agneau, oeufs, kiwi

---

## Voir aussi

+ [lecomptoirdici.fr/](https://www.lecomptoirdici.fr/) - vous allez pouvoir découvrir les produits de saison bio, super frais et hyper locaux, sélectionnés avec soin auprès de 35 producteurs, éleveurs et transformateurs avec lesquels nous avons choisi de travailler, au + près de lorient.
+ [mangeons-local.bzh/](https://www.mangeons-local.bzh/la-carte/)


## à trier

Minoterie Le Dérout — Farines, farines biologiques. Vente en directe aux particuliers.
(Attention Transformation) 10 rue du Bois d’Amour - 29930 Pont-Aven - Tél : 02 98 06 02 03 - Vente aux particuliers, farine et paillage. • Ouverture : du lundi au vendredi sauf mercredi après-midi
Le Safran des Hermines — Olivier Gourmelen

Lieu-dit Kernaour 29930 Pont-Aven Tél : 06 89 33 76 39 o.gourmelen29@gmail.com Safran des Hermines Safran, huile safranée, miel, bonbons, sirop de safran…. Vente sur place. Récolte en octobre. • Ouverture : toute l’année, le mercredi de 13h30 à 20h30 et sur rendez-vous

TERR’AVEN - Miel en rayon, safran, vinaigre de cidre, sel aux aromates...

SARL Le Naour Jean-Marie Le Naour Saint-Maudé
29930 Pont-Aven Tél : 06 78 18 35 98 labyrinthedepontaven@gmail.com Viande bovine blonde d’aquitaine élevée Sans OGM • Ouverture : vendredi soir et samedi matin selon commandes (1fois/mois)

La Basse-Cour Maude Rousseau Kercaudan
29930 Pont-Aven Tél : 02 98 71 51 30 ou 06 24 15 05 36 Toute l’année : poulets, pintades, canards, cailles, lapins, cochons. Et à Noël : dindes, oies, chapons, pintades chaponnées, faisans. • Ouverture : à la ferme le vendredi de 17h à 19 h et vente sur les marchés.

Le potager de saint'é
Trégunc 29910 Tel : 02 98 50 29 81 e-mail: phcamama@yahoo.fr

Ferme de Kersegalou — Produits laitiers et viande de la ferme
Située entre l’Aven et le Bélon, sur la commune de Riec-sur-Bélon, la ferme abrite un troupeau d’environ 85 vaches laitières et leur suite, auprès desquelles s’activent Vincent, Annie, Louis-Pierre et Odile. Vente à la ferme : Nos produits laitiers sont vendus à la ferme le vendredi de 16 à 19 h, ou en matinée sur 2 marchés : Trégunc le mercredi et Riec le samedi — Site www.

Les ruchers Marneffe & compagnie - Miel d'été et miel de printemps extrait et conditionnés sous vos yeux
Rozic, 29380 Le Trévoux - Téléphone : 0646267623 - geoffroy.marneffe@laposte.net. Vente de 16h à 19h à notre magasin le mardi et le vendredi.

bateau de pêche Zeus Faber — Petite pêche et vente directe sur Tregunc
Cale de Porz an Halen 29910 Tregunc (Attention Hors-jeu - 12km à partir du point le plus proche - Pont-Aven !) — Fred, 27 ans, va seul en mer tous les matins depuis fin 2017. Sur son ligneur polyvalent, le Zeus Faber, il pratique la petite pêche (pêche du jour, bateau de moins de 10 mètres et pêchant dans la zone des 6 miles). Gage de fraîcheur, les poissons et crustacés du jour sont proposés en vente directe. Fred propose aux consommateurs du poisson frais, de saison, sans intermédiaire et à prix fixe toute l’année. Selon la saison et pour les poissons, vous retrouverez des rougets, lottes, tacauds, merlans, etc. Pour les crustacés, des homards, langoustes, araignées et étrilles pêchés aux casiers. Retrouvez les prix sur le site www.

La Ferme de Kercabon Vras - Légumes, miels, oeufs. Paniers de légumes à récupérer lors de la vente à la ferme.
Kercabon Vras - 29380 - BANNALEC - Tél : 06 81 03 96 63 - silvent.robin@neuf.fr - Légumes, miels, oeufs. Paniers de légumes à récupérer lors de la vente à la ferme. Autres produits dans le panier: jus de pomme, œufs, soupes Jours d'ouverture le vendredi de 16h30 à 19h30. Retrouvez aussi ces produits : AMAP de Riec-sur-Belon.

AMAP de Riec-sur-Belon
