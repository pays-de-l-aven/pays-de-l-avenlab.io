---
layout: page.liquid
date: 2022-01-15
title: Freaks, un réseau résilient d'information locale
tags:
  - communication
  - numerique
  - donnees
  - reseau

featured: "home"

---

<p class="chapo">Si nous voulons que la communication distanciée continue de fonctionner dans un contexte d’accès limité à l’énergie (ou d'effondrement du réseau Internet), nous pouvons tirer d’importantes leçons des réseaux alternatifs.</p>

<!--more-->

-----

**Un appel à contribution pour [un premier prototype Freaks](https://gitlab.com/hentou/hentou.gitlab.io/-/wikis/Freaks) est lancé au sein de l'initiative [Hentoù](https://hentou.gitlab.io/).**

<figure>
<img src="freaks-proto-001.png" alt="Ébauche d'un premier prototype Freaks">
<figcaption>Ébauche d'un premier prototype Freaks — SMS/TXT &rarr; XML/RSS &rarr; Imprimante tickets</figcaption>
</figure>

-----

<mark>Cette page est une ébauche qui appelle à toute forme de contribution. </mark>

## Un internet low-tech

En guise d'introduction, voir les ressources suivantes:

+ [Comment créer un internet Low-Tech](https://solar.lowtechmagazine.com/fr/2015/10/how-to-build-a-low-tech-internet.html)
+ [Du low-tech numérique aux numériques situés](https://hal.archives-ouvertes.fr/hal-02867603/document) de Nicolas Nova et Gauthier Roussilhe

Les technologies à choisir sont très dépendantes des moyens à disposition et des informations à véhiculer. <mark>À développer</mark>.


## Freaks, une expérimentation minimaliste

(Cette expérimentation pourrait être menée dans le cadre des projets [Bretagne Transition](/articles/les-enjeux-de-bretagne-transition/))

Pour forcer l'approche minimaliste et garantir la production d'un prototype fonctionnel, Freaks veut se concentrer sur la diffusion de 2 types d'information entre les différents lieux/acteurs du territoire avant tout choix du moyen de communication.

1. J'ai besoin d'aide dans 3 jours
2. J'organise une fête le 16 décembre

La problématique à résoudre est la suivante: Comment assurer la diffusion d'un besoin d'aide ou de l'annonce d'une festivité entre les différents lieux/acteurs du territoire sans passer par les réseaux actuels: téléphone, Internet...?

la première réponse pourrait être: [se déplacer](/articles/mobilites-dans-le-territoire/) et en parler (et en profiter pour boire un coup)! Mais cette pratique ne tire pas partie des qualités d'un réseau maillé à diffuser l'information.

Comme dirait Philippe KAL-LoRa "mieux vaut prendre de la hauteur que de viser la puissance", donc si chaque lieu/acteur du territoire disposait d'un observatoire, nous pourrions à l'instar du [code international des signaux maritimes](https://fr.wikipedia.org/wiki/Code_international_des_signaux_maritimes) afficher un drapeau, comme par exemple *Victor* pour la demande d'assistance. Cette référence est un exemple d'une information claire et concise à laquelle il faudrait rajouter : 1 - une notion de temporalité (dans 3 jours, le 16 décembre...) et 2 - l'identification du lieu/acteur indispensable au moment de la diffusion de l'information.

<figure>
<img src="drapeaux-01.png" alt="2 exemples d'échange d'information à vue à l'aide de drapeaux">
<figcaption>Dans le cas présent, il serait plus interessant d'utiliser des lettres pour le lieu/acteur - KY pour Kerminy et AZ pour atelier Z. Le 47.5 traduit le 5e jour de la 47e semaine de l'année en cours. Attention les informations données ici ne sont pas vraies - il n'y a pas de fête à l'Atelier Z le 16 décembre. Et le drapeau "festivité" n'existe pas dans le code international des signaux maritimes.</figcaption>
</figure>

Nous pourrions aussi penser à l'[alphabet sémaphore](https://fr.wikipedia.org/wiki/Alphabet_s%C3%A9maphore) ou au télégraphe de Chappe. Voir l'expérience du [Chappeduino](http://toysfab.com/2019/02/le-chappeduino-un-telegraphe-de-chappe-commande-par-arduino/) pour continuer à jouer un peu avec le numérique.

Quelque soit le code, il est interessant de pouvoir simplifier l'information comme suit :

+ "Besoin d'assistance à Kerminy le 26 novembre" &rarr; "xKY475"
+ "Fête à l'atelier Z le 16 décembre" &rarr; "+AZ504".

<mark>Voir comment pourrait se faire la diffusion de ce type d'information visuelle, non pas d'un point A à un point B mais sur l'ensemble du maillage. Comment un lieu/acteur pourrait-il à la fois émettre et transmettre ?</mark>


## Mailler le territoire

Si nous continuons à filer la métaphore du sémaphore - ou du télégraphe de Chappe, nous pouvons nous poser la question de la distance "à vue" et donc du maillage ainsi obtenu. Pour information, en 1793, Chappe mène une première expérimentation entre Ménilmontant et Saint-Martin-du-Tertre - soit une trentaine de kilomètres avec un relai à Écouen. La ligne Lille-Paris est achevée en juillet 1794. Ses quinze stations, distantes de 14,6 kilomètres en moyenne, sont le plus souvent établies sur des tours ou des clochers d’église.

### Le réseau-relief du territoire

Ci-dessous les points d'altitude de chaque partie du territoire. <mark>À vérifier *de visu*</mark>. La distance entre chaque point proche est d'environ 5 kilomètres.

<figure>
<img src="reseau-relief-01.png" alt="Un exemple de réseau-relief sur le territoire">
<figcaption>Un exemple de réseau-relief sur le territoire avec, sur les pastilles, l'altitude en mètres (les montagnes bretonnes sont âgées de plus de 300 millions d’années - d'où une certaine usure.)</figcaption>
</figure>

L'enjeu est de pouvoir faire correspondre l'ensemble des lieux/acteurs avec l'un des points du maillage pour permettre la diffusion et la réception d'un message.

### les autres points hauts du territoire   

+ Le clocher des églises et chapelles
+ Les chateaux d'eau
+ ~~Les antennes 5G~~
+ Les séquoias géants de Kerminy
+ ...


## Le besoin d'une technologie frugale

Nous pouvons hisser des drapeaux mais nous pouvons aussi mettre en place un réseau économe en ressources pour faciliter la diffusion de l'information.

Une équipe du Konk-Ar-Lab travaille actuellement sur la mise en place d'un réseau LoRaWAN afin de diffuser des informations venant de capteurs de température, de particules fines, de CO2... LoRaWAN est un protocole de télécommunication permettant la communication à bas débit, par radio, d'objets à faible consommation électrique communiquant selon la technologie LoRa et connectés à l'Internet via des passerelles, participant ainsi à l'Internet des objets.

L'avantage de la technologie LoRa est qu'elle demande peu d'énergie pour être alimentée, l'autre avantage est le besoin d'être succinct dans l'information à diffuser. Il pourrait être intéressant de collaborer pour pouvoir échanger l'information sur le besoin d'aide et les fêtes organisées.

<mark>Note: Faire un point sur l'ambiguité juridique à utiliser LoRa pour faire passer une communication de personne à personnes de type "J'ai besoin d'un coup de main dans 3 jours" ou "Jam session à la prochaine pleine lune".</mark>

Il s'agirait de concevoir une interface simple pour saisir des information basiques. Une autre fonctionnalité pourrait permettre de proposer son aide à une demande : 475KL>KY (l'information diffusée devrait sans doute commencer par la date) signifie que le Kon-Ar-Lab viendra filer un coup de main à Kerminy le 26 novembre.

## Un résumé imprimé des besoins et festivités

Une fois l'information diffusée, transmise et reçue, chaque lieu/acteur doit pouvoir la consulter. Imaginons l'impression d'un résumé quotidien des besoins et festivités à venir (Voir [tinyprinter.club/](https://tinyprinter.club/). Cette impression est donnée à lire et invite à l'échange oral. Cette information pourrait être bien-sur couplée avec l'ensemble des informations venant de différents capteur (qualité de l'air, températures...).

<figure>
<img src="little-printer.png" alt="Photo de Little Printer">
<figcaption>Little Printer était une petite imprimante thermique lancée en 2012 par l'entreprise BERG qui permettait de recevoir tous types d'informations.</figcaption>
</figure>

Des solutions écrans peuvent être aussi imaginées. Et aussi des objets avec LEDs pour une information affichée et interactive.

### L'inclusion de l'ensemble des citoyens

Penser l'inclusion avec une diffusion plus large sous forme de livret imprimé ou d'affichage "libre".

<figure>
<img src="affichage-associatif-tregunc-01.png" alt="Photo d'un panneau d'affichage libre à Tregunc">
<figcaption>Exemple d'affichage libre à . Vous pouvez retrouvez une liste de l'implantation de ces panneaux sur la page <a href="/articles/des-signes-dans-l-espace-du-territoire/" alt="">Des signes dans l'espace du territoire</a>
</figcaption>
</figure>

## Quelques ressources

+ [Briar Project](https://briarproject.org/how-it-works/) - Briar is a messaging app designed for activists, journalists, and anyone else who needs a safe, easy and robust way to communicate.
+ [Beaker Browser](https://beakerbrowser.com/) - An experimental peer-to-peer Web browser.

## Contact

Pour toute proposition de contribution : [pyc@lieu-subjectif.com](mailto:pyc@lieu-subjectif.com)

Et merci à Philippe pour ses sources sur le télégraphe de Chappe.

![Affichage libre](affichage-crypto.png)
