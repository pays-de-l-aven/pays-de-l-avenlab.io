---
layout: page.liquid
date: 2022-06-01
title: Un début de réponse à un appel à projet numérique du Finistère
tags:
  - numerique
  - acteurs
  - donnees
  - mobilite

---

Ce texte était un début de réponse à l’appel à projets Numérique 2022 lancé par le Conseil départemental du Finistère qui se donne comme objectif d’accélérer la transition numérique du Finistère et d’accompagner l’arrivée du très haut débit en développant les usages du numérique.

<!--more-->

&rarr; Voir l'[Appel-a-projets-numeriques-2022](https://www.finistere.fr/Actualites/Appel-a-projets-numeriques-2022)

Les projets soutenus dans le cadre de cet appel à projets doivent contribuer au développement d’une démarche d’inclusion numérique à travers des actions locales.

2 thématiques:

1. Sensibiliser, encourager et favoriser l’appropriation des usages numériques
2. Simplifier le quotidien

Les projets seront analysés selon 3 critères :

- Favoriser le développement des usages du numérique : pertinence de
la proposition au regard des orientations de l’appel à projets, prise en compte du public ciblé (les personnes éloignées du numérique).
- S’inscrire à l’échelle d’un territoire : ancrage local, être au service d’un territoire, rayonnement possible du projet.
- Innover dans les actions et propositions : les actions soutenues
doivent être innovantes, c’est-à-dire ne pas répliquer des actions déjà
réalisées par le porteur. Les actions peuvent toutefois s’inspirer de
projets déjà réalisés par d’autres et les adapter au contexte
territorial.

Mais avant de pouvoir répondre à cet appel à projet, il faut déterminer les véritables usages et besoins numériques pour l’ensemble des citoyens d’un territoire donné.

## Besoins des citoyens et rôle du numérique

Comment identifier les besoins élémentaires des habitants d’un territoire et en quoi les usages numériques peuvent-ils contribuer à répondre à ces besoins? S’il existe une litterature scientifique sur ces besoins dans le cadre urbain, elle est beaucoup plus rare quand ces besoins concerne l’espace rural. Mais reprenons les thématiques de la ville du 1/4 d’heure pour tenter d’analyser les besoins pour les besoins suivants : habiter, travailler, s'approvisionner, se soigner, s'éduquer et s'épanouir.

### Habiter

Le numérique est une aide précieuse pour la recherche de logement et pour ensuite gérer l’ensemble des démarches liées à son occupation - taxes, assurances, demande d’aides...

Mais les freins de l’accès au logement restent trop nombreux et si le numérique peut contribuer à en minimiser certains, ce n’est pas la panacée.

→ Trouver une liste des vulnérabilités sur le logement

### Travailler

Il est même aujourd’hui impossible de faire toutes les démarches liées à la recherche d’emploi sans avoir une connaissance des outils numériques - infernale impasse pour ceux qui n’ont pas eu cette chance d’être formés au numérique.

→ Trouver une liste des vulnérabilités sur le travail

### S'approvisionner

Le numérique prévilégie les grands acteurs comme AZ et n’invite pas les citoyens à s’approvisionner localement.

→ Trouver une liste des vulnérabilités sur l’approvisionnement.

### Se soigner

Est-ce que la consultation à distance peut être une réponse au suivi de certaines pathologies?

→ Trouver une liste des vulnérabilités sur la santé et le soin.

### S'éduquer

L’internet est une magnifique base de connaissance pour ceux qui ont les moyens de ne pas se perdre dans cette somme d’informations. Les domaines et sujets des sciences et de l’art sont très bien documentés et accessibles mais doivent s’accompagner d’un mentorat bienveillant. Quel rôle pour la formation initiale et la formation continue?

→ Trouver une liste des vulnérabilités sur l’éducation.

### S'épanouir.

→ Trouver une liste des vulnérabilités sur l’épanouissement personnel.

## Le besoin de mobilité avant le besoin de numérique

Même si le numérique joue aujourd’hui un rôle clé pour la satisfaction des besoins élémentaires, l’utilisation de la voiture en milieu rural est bien plus indispensable. Quelles réflexions mener pour combiner et repenser ces 2 moyens d’action? Comment penser la mobilité pour repenser le numérique et comment penser le numérique pour repenser la mobilité?

## Voir aussi

Les besoins fondamentaux de Max-Neef dans l’article *Une erreur de "tech"* de Gauthier Roussilhe.

[Une erreur de 'tech'](https://gauthierroussilhe.com/post/erreur-tech.html)

## Un numérique à l’origine d’exclusions sociale

### Un numérique trop présent

Il est aujourd’hui impossible de faire la moindre démarche auprès des différentes institutions publiques. Cette tendance n’est lepas motivée par à un désir d’accessibilité mais par le besoin de diminuer le nombre d’agents du service public. Si de gros efforts sont entrepris sur la simplification de ces démarches, les plus démunis sont encore plus tenus à l’écart de recours possibles.

### La fracture numérique ou “illectronisme”

Aujourd’hui, près de 13 millions de personnes déclarent rencontrer des difficultés dans l’usage des outils numériques, selon le Défenseur des droits. L’Institut national de la statistique et des études économiques (Insee), lui, estime que 16,5 % de la population souffre d’« illectronisme », c’est-à-dire d’incapacité à se servir des outils numériques.

Il est indispensable pour les territoires de tout mettre en œuvre pour réduire cette fracture numérique.

## Réduire la fracture numérique dans le contexte de la neutralité carbone

Le besoin de sobriété numérique peut être une chance pour repenser les besoins, les usages et tenter de réduire la fracture sociale.

### Le contexte environnemental nous oblige à réduire l’empreinte du numérique

Selon le Shift Project, le numérique qui est au cœur de l’ensemble de nos systèmes et un atout sentral pour traverser surmonter les crises à venir est incompatible avec une trajectoire 2°C. Avec 6% par an de croissance, la part du numérique de ses emissions de GES mondiales est déjà de 3.5% est pourrait doubler en 2025.

[Impact environnemental du numérique et gouvernance de la 5G](https://theshiftproject.org/article/impact-environnemental-du-numerique-5g-nouvelle-etude-du-shift/)

### Penser sobriété pour redéfinir les usages

L’Association négaWatt, qui regroupe des professionnels de l’énergie et des citoyens, a pour objectif le développement d’une politique énergétique fondée sur la sobriété et l’efficacité énergétique. L’association définie plusieurs type de sobriété:

- La sobriété dimensionnelle : en privilégiant un équipement adapté aux besoins.
- La sobriété coopérative : en mutualisant les usages.
- La sobriété d’usage : en gérant raisonnablement l’utilisation des appareils et des biens.
- La sobriété organisationnelle : en structurant différemment les activités dans l’espace et le temps.
- La sobriété matérielle : en diminuant la consommation de biens et de produits matériels

## La sobriété numérique comme dynamique territoriale

Le projet suivant est un début de réponse à l’appel à projet du département Finistère et prend comme espace d’expérimentation le territoire de la CCA (Concarneau Cornouaille Agglomération).

### Une problématique pour le territoire de la CCA

**Comment la dynamique numérique locale et durable peut faciliter la satisfaction des besoins les plus élémentaires de l’ensemble des citoyens?**

### Quelques réponses en toute sobriété

Nous reprenons ici les différents types de sobriété défini par l’Association négaWatt.

**La sobriété dimensionnelle**

La compréhension du fonctionnement d’un outil pour une action donnée permet de l’adapter au mieux à l’action. Ce qui semble trivial pour un marteau apparaît beaucoup plus compliqué pour un outil (ou un appareil) numérique. Ce temps de la réflexion pour ajuster au mieux l’outil à la finalité de l’action permet de mieux définir l’objectif, les moyens mis en œuvre et le recours possible au numérique. Avant d’apprendre à utiliser les outils numériques, il est important de comprendre pourquoi nous devrions avoir recours à ces outils.

Le wiki Low-Tech Lab est un parfait exemple qui montre que le numérique n’est pas une fin en soi mais qu’il peut permettre de diffuser ces idées et savoirs-faire. La question reste de savoir de quoi chacun a besoin, a minima, pour pouvoir accéder à cette base de connaissance.

**La sobriété coopérative**

Si l’économie de marché encourage les usages individuées, la sobriété n’est possible qu’en mutualisant les actions. La coopération est ce qui permet les dynamique pour un numérique ouvert et libre sont possibles. L’inclusion passe par cette compréhension de ces modes de coopération. Cette coopération peut se faire en ligne mais ne doit à aucun moment faire l’impasse sur le besoin de se retrouver dans un espace physique.

**La sobriété d’usage**

Une utilisation raisonnable des appareils et des biens.

**La sobriété organisationnelle**

En structurant différemment les activités dans l’espace et le temps.

**La sobriété matérielle**

Les points précédents permettent la pensée et l’utilisation de matériels de récupération. Coopération et organisation doivent permettre la récupération, la réparation et l’entretien des matériels mis à disposition.

## Une trame conviviale ou réseau de pair à pair

La résonnance entre le besoin d’un réseau serré d’entraide et l’émergence d’un réseau numérique local ne fait que souligner ce besoin de complémentarité.



---

# Références

[Emmaüs Connect](https://emmaus-connect.org/)

[Illectronisme les laissés-pour-compte du tout-numérique.pdf](Appel%20a%CC%80%20projet%20nume%CC%81rique%2045268fbab8f340d8880f2c9cfe7e9a8e/Illectronisme_les_laisses-pour-compte_du_tout-numerique.pdf)

[au secours des naufragés de la start-up nation.pdf](Appel%20a%CC%80%20projet%20nume%CC%81rique%2045268fbab8f340d8880f2c9cfe7e9a8e/au_secours_des_naufrages_de_la_start-up_nation.pdf)

[Bienvenue sur le site du collectif CHATONS | CHATONS](https://www.chatons.org/)
