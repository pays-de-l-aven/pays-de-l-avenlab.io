---
layout: page.liquid
title: La difficile identification des lieux acteurs

tags:
  - personnes
  - lieux
  - organisations
---

<p class="chapo">À chaque nouvel annuaire, ses définitions, critères et technologies. Ils existent de nombreux projets qui cherchent à identifier les lieux acteurs mais par manque de concertation, ils restent incomplets. Un des chantiers à mener par le territoire est l'ouverture et la mutualisation de ces données.</p>

<!--more-->

## Qu'est-ce qu'un tiers-Lieux

La définition donnée par France Tiers-Lieux est la suivante: <q>Un tiers-lieu est une démarche collective d’intérêt général, qui s’inscrit dans la coopération territoriale dès sa conception. Par nature uniques, ils sont non réplicables. Ils émanent d’un collectif d’acteurs, qui ensemble souhaitent créer de nouvelles dynamiques économiques ou sociales en réponse aux enjeux de leur territoire. C’est en réunissant les habitants et futurs usagers du lieu, dessinant ainsi sa communauté active, que les activités du lieu vont se définir et que le projet va s’ajuster.</q>

Il est dommage qu'il n'y ait pas, dès la création d'un tiers-lieu, un accompagnement visant à la production de [données interopérables](/articles/des-donnees-interoperables/) et ouvertes pour faciliter son identification.

## Les annuaires disponibles

### Transiscope

Transiscope annonce la couleur: "Aujourd’hui, de nombreux d’acteurs de la transition et des alternatives ont entamé un travail de recensement et de cartographie de leurs organisations, actions et écosystèmes. Dans la majorité des cas, néanmoins, ces informations sont éparpillées sur les sites de chacune de ces organisations et les données ne peuvent pas communiquer entre elles en raison de choix techniques différents : aucune visualisation agrégée n’était jusqu’à présent possible. Pour permettre de relier ces alternatives, une dizaine de collectifs travaillent depuis deux ans pour développer des outils libres permettant de connecter les différentes bases de données existantes et de les visualiser au même endroit : TRANSISCOPE".

Voir [les acteurs du territoire recencés sur Transiscope](https://transiscope.org/carte-des-alternatives/#/carte/@47.888,-3.823,12z?cat=all
).


### France Tiers-Lieux

Sur le site de l’Association nationale des Tiers-Lieux [francetierslieux.fr](https://cartographie.francetierslieux.fr/), seuls sont référencés pour le pays de l'Aven : l'[Atelier Z](https://cartographie.francetierslieux.fr/?_ga=2.31993403.466020347.1633433732-1412926416.1632569176#?preview=organizations.6151e9c6f130ae407713fdcd), le [Konk Ar Lab](https://cartographie.francetierslieux.fr/?_ga=2.31993403.466020347.1633433732-1412926416.1632569176#?preview=organizations.6018f9de69086482678b4b1d), le [centre socioculturel La Balise](https://cartographie.francetierslieux.fr/?_ga=2.31993403.466020347.1633433732-1412926416.1632569176#?preview=organizations.6018f9de69086482678b4b22) et l'[Ebus itinérant de Concarneau](https://cartographie.francetierslieux.fr/?_ga=2.31993403.466020347.1633433732-1412926416.1632569176#?preview=organizations.6018f9de69086482678b4b27). Lire aussi le [Rapport Tiers-Lieux 2021/](https://francetierslieux.fr/rapport-tiers-lieux-2021/)

### Movilab, l'encyclopédie des tiers-lieux

[Movilab](https://movilab.org/) est (initialement) un dispositif d’incubation visant à mettre en place des laboratoires de modes de vie durables in vivo en partenariat avec des territoires. Ce dispositif se fonde sur la combinaison des cultures du logiciel libre et des pensées écologiques, et s'appuie sur la pratique (sociale) des Tiers-Lieux. L'ambition est de faciliter la conversion vers des modes de vie durables par l'expérimentation et le partage des savoirs. Lire sur Movilab, la présenettaiion de [Atelier Z](https://movilab.org/wiki/Atelier_z), l'[étude et Proposition d'un panel de low-tech pour cet atelier](https://movilab.org/wiki/Bretagne_Transition_/_Etude_et_Proposition_d%27un_panel_de_low-tech_pour_l%27atelier_Z) ainsi que la présentation de l'association [Bretagne Transition](https://movilab.org/wiki/Bretagne_Transition).

---

### Voir aussi

+ [La Coopérative Oasis](https://cooperative-oasis.org/) accompagne celles et ceux qui vivent ou souhaitent vivre dans des écolieux collectifs
+ [La carte des Hameaux Légers](https://hameaux-legers.gogocarto.fr/annuaire#/carte/melgven@47.923,-3.793,11z?cat=all)
+ [Commune Mesure](https://communemesure.fr/)
+ [Les tiers-lieux nourriciers](https://nourriciers.tierslieux.net/)
