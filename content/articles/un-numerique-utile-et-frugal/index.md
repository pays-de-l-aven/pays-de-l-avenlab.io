---
layout: page.liquid
date: 2022-06-06
title: Un numérique utile et frugal
tags:
  - numerique
  - acteurs
  - donnees
  - mobilite

featured: "home"

---

Une petite note d'intention en pensant à la journée "Démystifier le numérique" avec l'association des Portes Logiques à l'Atelier Z de Névez.

<!--more-->

## Besoins des citoyens et rôle du numérique

Comment identifier les besoins élémentaires des habitants d’un territoire et en quoi les usages numériques peuvent-ils contribuer à répondre à ces besoins ? S’il existe une littérature scientifique sur ces besoins dans le cadre urbain, elle est beaucoup plus rare quand ces besoins concerne l’espace rural. Nous pouvons reprendre les thématiques de la ville du 1/4 d’heure pour tenter d’analyser le recours possible au numérique pour les besoins suivants : habiter, travailler, s'approvisionner, se soigner, s'éduquer et s'épanouir.

## Le besoin de mobilité avant le besoin de numérique

Même si le numérique joue aujourd’hui un rôle clé pour la satisfaction des besoins élémentaires, l’utilisation de la voiture en milieu rural est bien plus indispensable. Quelles réflexions mener pour combiner et repenser ces 2 moyens d’action? Comment penser la mobilité pour repenser le numérique et comment penser le numérique pour repenser la mobilité?

## Un numérique à l’origine d’exclusion sociale

Il est aujourd’hui impossible de faire la moindre démarche auprès des différentes institutions publiques sans avoir recours au numérique. Si de gros efforts sont entrepris sur la simplification de ces démarches, les plus démunis sont encore plus tenus à l’écart de recours possibles. Aujourd’hui, près de 13 millions de personnes déclarent rencontrer des difficultés dans l’usage des outils numériques, selon le Défenseur des droits. L’Institut national de la statistique et des études économiques (Insee), lui, estime que 16,5 % de la population est dans l'incapacité à se servir des outils numériques. Il est indispensable pour les territoires de tout mettre en œuvre pour réduire cette fracture numérique.

## Réduire la fracture numérique dans le contexte de la neutralité carbone

Le besoin de sobriété numérique peut être une chance pour repenser les besoins, les usages et tenter de réduire la fracture sociale. Selon le Shift Project, le numérique qui est au cœur de l’ensemble de nos systèmes et un atout central pour traverser surmonter les crises à venir est incompatible avec une trajectoire 2°C. Avec 6% par an de croissance, la part du numérique de ses emissions de GES mondiales est déjà de 3.5% est pourrait doubler en 2025.

Il est important de penser sobriété pour redéfinir les usages. L’Association négaWatt, qui regroupe des professionnels de l’énergie et des citoyens, a pour objectif le développement d’une politique énergétique fondée sur la sobriété et l’efficacité énergétique. L’association définie plusieurs type de sobriété:

- La sobriété dimensionnelle : en privilégiant un équipement adapté aux besoins.
- La sobriété coopérative : en mutualisant les usages.
- La sobriété d’usage : en gérant l’utilisation des appareils et des biens.
- La sobriété organisationnelle : en structurant les activités dans l’espace et le temps.
- La sobriété matérielle : en diminuant la consommation de biens et de produits matériels

## La sobriété numérique comme dynamique territoriale

La problématique et les réponses à suivre ne sont que des ébauches. À développer.

### Problématiques pour le territoire

<mark>Comment la dynamique numérique locale et durable peut faciliter la satisfaction des besoins les plus élémentaires des citoyens ?</mark>

<mark>Comment la satisfaction des besoins élémentaires citoyens peut appeler une dynamique numérique locale et durable</mark>

### Quelques réponses en toute sobriété

Nous reprenons ici les différents types de sobriété défini par l’Association négaWatt.

**La sobriété dimensionnelle** - La compréhension du fonctionnement d’un outil pour une action donnée permet de l’adapter au mieux à l’action. Ce qui semble trivial pour un marteau apparaît beaucoup plus compliqué pour un outil (ou un appareil) numérique. Ce temps de la réflexion pour ajuster au mieux l’outil à la finalité de l’action permet de mieux définir l’objectif, les moyens mis en œuvre et le recours possible au numérique. Avant d’apprendre à utiliser les outils numériques, il est important de comprendre pourquoi nous devrions avoir recours à ces outils.

Le wiki Low-Tech Lab est un parfait exemple qui montre que le numérique n’est pas une fin en soi mais qu’il peut permettre de diffuser idées et savoirs-faire. La question reste de savoir de quoi chacun a besoin, a minima, comme appareillage numérique pour pouvoir accéder à cette base de connaissance.

**La sobriété coopérative** - Si l’économie de marché encourage les usages individuées, la sobriété n’est possible qu’en mutualisant les actions. La coopération est ce qui permet les dynamiques pour un numérique ouvert et libre d'exister. L’inclusion passe par cette compréhension de ces modes de coopération. Cette coopération peut se faire en ligne mais ne doit à aucun moment faire l’impasse sur le besoin de se retrouver dans un espace physique.

**La sobriété d’usage** - Un appareillage numérique devrait pouvoir aider à la réalisation d'une action clairement identifiée et uniquement celle-là, sans ajouter de la complexité à ce que pourrait être cette même action sans le recours au numérique.

**La sobriété organisationnelle** - Pour structurer différemment les activités dans l’espace et le temps, une infrastructure numérique peut aider à embrasser cette apparente complexité. Malheureusement aujourd'hui la multiplications des outils de messagerie et des outils collaboratifs qui ne sont pas interopérables ajoute encore de la complexité. Il est important d'envisager de [fuir le *Cloud*](https://fuir-le-cloud.gitlab.io/) (autre projet encore en cours d'écriture).

**La sobriété matérielle** - Les points précédents permettent la pensée et l’utilisation de matériels de récupération. Coopération et organisation doivent permettre la récupération, la réparation et l’entretien des matériels mis à disposition.

## Une trame conviviale ou réseau de pair à pair

La résonance entre le besoin d’un réseau serré d’entraide et l’émergence d’un réseau numérique local ne fait que souligner ce besoin de complémentarité. Un projet inclusif d'identification et de diffusion des actions du territoire pour répondre à l'ensemble des besoins élémentaires tout en interrogeant le besoin du recours au numérique, permettrait de documenter et d'expérimenter objectifs et moyens mis en œuvre.

Voir les articles :

- [Freaks, un réseau résilient d'information locale](/articles/freaks-un-reseau-resilient-d-information-locale/)
- [Un maillage convivial](/articles/un-maillage-convivial/)
