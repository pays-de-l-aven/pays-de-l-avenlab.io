---
layout: page.liquid
date: 2022-04-15
title: Arpenter le territoire
tags:
  - acteurs
  - lieux

featured: "home"
---

<p class="chapo">Ce projet d'arpentage consiste à explorer le paysage pour et grâce à la production de données et d'analyser le rapport étroit entre activités et paysage.</p>

<!--more-->

<mark>Revoir cette page pour plus se concentrer sur les entretiens qui permettraient à chaque "territorien.ne" de décrire son territoire</mark>

<figure>
  <blockquote cite="https://ouatterrir.fr/index.php/projet-pilote/">
    <p>Nul autre que le citoyen n’est en mesure d’explorer et de décrire ce à quoi il est réellement attaché. Et sans cette auto-description, point de compréhension réelle du territoire vécu.</p>
  </blockquote>
  <figcaption>—Bruno Latour</figcaption>
</figure>

Le [Low-tech Tour Californouaille](https://lowtechlab.org/fr/actualites-blog/low-tech-tour-2021) réalisé par l’équipe du Low-tech Lab sur le territoire de Concarneau et ses alentours, en juillet 2021 est un bon exemple d'arpentage. Voir [la vidéo](https://youtu.be/AW0pX2fj9yI) de l'expédition.

Ce projet d'arpentage est une réponse à [la difficile identification des tiers-lieux](/articles/la-difficile-identification-des-tiers-lieux/) et pourrait être un des [enjeux de Bretagne Transition](/articles/les-enjeux-de-bretagne-transition/) pour la production de [données interopérables](/articles/des-donnees-interoperables/) selon [une ontologie](/articles/l-ontologie-du-pays-de-l'aven/) définie.

## Une série d'entretiens et d'ateliers pour la récolte de données

→ Voir la page [Entretiens et ateliers](/articles/entretiens-et-ateliers/)

## Une nouvelle cartographie du paysage

à définir

## références

-   [Projet pilote "où atterrir"](https://ouatterrir.fr/index.php/projet-pilote/)
