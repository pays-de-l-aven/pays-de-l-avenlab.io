---
layout: page.liquid
date: 2021-12-01

title: L'ontologie du pays de l'Aven
tags:
  - donnees

featured: "home"

---

<p class="chapo">Sur la base de l’<a href="https://www.virtual-assembly.org/ontologie-pair/">ontologie PAIR</a> de l’Assemblée Virtuelle et de ses partenaires, une première proposition peut être faite. Cette proposition n'est en rien exhaustive et/ou définitive. Avant de pouvoir mettre en place un système Semapps ou Solid, l'ensemble des données pourraient être agrégées dans une base <s>Airtable</s> Baserow.io (Débattre de la pertinence d'un solution Cloud). Le projet <a href="/articles/arpenter-le-territoire/">Arpenter le territoire</a> pourrait permettre la collecte de données.</p>

<!--more-->

## les différents concepts et leurs relations

### Individu

un `individu` est une personne physique, avec un prénom et un nom (et un numéro de sécurité sociale?). Cet `individu` peut avoir une adresse mail et un numéro de téléphone.

### Groupe

Un `groupe` défini tout type d'organisation, qu'il s'agisse d'entités juridiques ou non, d'entreprises, d'institutions ou de regroupement sans statut. Un `groupe` se compose d'au moins 2 `individu`.

### Évenement

Un `evenement` est une réunion d'un `groupe` dans un ou plusieurs `espace` donnés. Cet `evenement` a une `date` de début et une `date` de fin.

### Espace

Un `espace` peut être géolocalisé ou virtuel (ex.: webinaire).

### Projet

Un `projet` est porté soit par un `groupe` soit par un `individu`. Tout `projet` doit être consultable dans un `document`.

### Document

Un `document` est un ensemble structuré de signes et/ou de données.

### Thème

Un `theme` défini la nature, le contexte, les enjeux d'un `projet`, d'un `evenement`, d'un `groupe`... Il est par nature subjectif.

### Idée

Une `idee` est une intention de `projet`.

### Ressource

Une `ressource` est une mise à disposition par un `individu` ou un `groupe` de compétences, connaissances, outils, financements... afin de combler un `besoin` identifié au sein d'un `projet`.

### Besoin

Un `besoin` est l'expression d'un manque dans le cadre d'un `projet`. Un `besoin` est satisfait par une `ressource`. Ce `besoin` peut être financier, conceptuel, humain...

### Vulnérabilité

Une `vulnerabilite` met en lumière les risques encourus par un `projet`, un `groupe`, un `individu`... au sein du territoire.
