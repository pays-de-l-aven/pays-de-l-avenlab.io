---
layout: page.liquid
date: 2022-04-15
title: Les artefacts emblématiques du territoire
tags:
  - projets
  - objets
  - mobilite
  - logistique
  - habitat

featured: "home"

---

<p class="chapo"> Le déploiement d'objects à l'échelle du territoire pourrait aussi être les vecteurs et ambassadeurs de cette dynamique de transformation.</p>

<!--more-->

## Les artefacts du patrimoine

Certaines constructions ponctuent déjà le paysage breton et pourraient jouer un nouveau rôle dans le besoin de transition. Exemple: les fours à pain, les menhirs, les moulins, les chapelles, les calvaires...


## Des artefacts low-tech au service d'une mobilité et d'une logistique verte

<figure>

<img src="/articles/les-artefacts-emblematiques-du-territoire/revolta-et-remorque-001.png" alt="Revolta et remorque">

  <figcaption>La rosalie Revolta et la remorque partagée 300kg à assistance électrique pour se déplacer et transporter sur l'ensemble du territoire.</figcaption>
</figure>

## Des nouveaux refuges

Un projet de hutte

<figure>

<img src="/articles/les-artefacts-emblematiques-du-territoire/hutte-kerminy-proto-ok-exterieur-001.png" alt="Prototype de hutte à Kerminy">

  <figcaption>Le prototype de hutte que nous pourrions voir fleurir sur l'ensemble du territoire.</figcaption>
</figure>
