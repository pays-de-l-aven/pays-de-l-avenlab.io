---
layout: page.liquid
title: Robinsonnades et besoins humains fondamentaux
tags:
  - acteurs
  - references

---

<p class="chapo">La robinsonnade est un genre littéraire et cinématographique, qui tient son nom du roman <cite>Robinson Crusoé</cite> de Daniel Defoe, Le héros doit alors improviser les moyens de sa propre survie dans un univers inconnu. Cette page liste, dans un premier temps, ces différents moyens de subsistance pour ensuite initier une réflexion sur les besoins humains fondamentaux.</p>

<!--more-->

L'idée ici est d'analyser dans chacune des œuvres, la partie de où les protagonistes s'organisent pour leur survie et non pas de discourir sur le caractère colonialiste évident de ces œuvres.

## Les robinsonnades les plus célèbres

<Mark>À compléter</Mark>

+ Robinson Crusoé - Roman de Daniel Defoe - 1719
+ Le Robinson suisse ou Journal d'un père de famille naufragé avec ses enfants - Roman de Johann David Wyss - 1812
+ L'Île mystérieuse - Roman de Jules Verne - 1874-1875
+ L'École des Robinsons - Roman de Jules Verne - 1882
  + [L'école des Robinsons sur wikisource](https://fr.wikisource.org/wiki/L%E2%80%99%C3%89cole_des_Robinsons)
+ Deux ans de vacances - Roman de Jules Verne - 1888
  + [Deux_Ans_de_vacances sur wikisource](https://fr.wikisource.org/wiki/Deux_Ans_de_vacances)
+ Seul au monde - film réalisé par Robert Zemeckis - 2000
+ Lost - Série - 2004 &rarr; 2010
+ Seul sur Mars - film réalisé par Ridley Scott, sorti en 2015
+ Arctic - film réalisé par Joe Penna, sorti en 2018.

Faire un point sur le concept *inverted Crusoeism* de J. G. Ballard (cf. Concrete Island)
