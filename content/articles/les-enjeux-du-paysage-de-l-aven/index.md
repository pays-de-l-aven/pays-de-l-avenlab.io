---
layout: page.liquid
title: Les enjeux du paysage de l'Aven
tags:
  - places
  - donnees
  - paysage
---

À partir des trois atlas de paysage bretons, l'[OEB](https://bretagne-environnement.fr/) a inventorié et synthétisé les différents enjeux à prendre en compte dans chaque unité de paysage. Ces indicateurs constituent [le référentiel régional des enjeux de paysage](https://bretagne-environnement.fr/suivi-enjeux-paysage-bretagne-datavisualisation) qui a pour but d’aider les collectivités territoriales à se saisir de la question paysagère.
