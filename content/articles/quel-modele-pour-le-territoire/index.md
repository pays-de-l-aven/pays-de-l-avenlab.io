---
layout: page.liquid
title: Quel modèle pour le territoire ?
tags:
  - données
---

+ Suivre le cours de [Modélisation et systèmes d’aide à la décision en environnement](https://env6008.teluq.ca/partie-i/) d'Élise Filotas



## Voir aussi

+ [Citego](https://www.citego.org/)
