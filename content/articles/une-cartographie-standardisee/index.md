---
layout: page.liquid
date: 2022-02-15

title: Une cartographie standardisée
tags:
  - cartographie
  - donnees
  - acteurs

featured: "home"

---

<p class="chapo">Le travail de cartographie peut aider à la définition d'un commun. La production de données standardisées peut permettre la lisibilité de ce commun au sein d'un territoire plus étendu et engager d'autres individus à participer à sa documentation et/ou en bénéficier. Les données aujourd'hui accessibles ont un caractère pratique, elles ne proposent pas encore une nouvelle pensée du paysage.</p>

<!--more-->

<mark>Note: les données cartographiques ne sont qu'un sous-ensemble d'une réflexion plus globale sur la [standardisation et l'interopérabilité des données](/articles/des-donnees-interoperables/).</mark>


### Voir

+ [Carto de la CCA](https://carto.cca.bzh/) basée sur le travail et les outils de [GeoBretagne](https://geobretagne.fr)
+ [kartenn.region-bretagne.fr/](https://kartenn.region-bretagne.fr/).
+ [https://transiscope.org/](https://transiscope.org/) - Le portail d’accès unique aux projets de la transition écologique et sociale !
+ [Test gogocarto](https://cornouaille.gogocarto.fr/)
+ [bretagne-environnement.fr/](https://bretagne-environnement.fr/)
+ [Cartographie Bretagne Transition](http://umap.openstreetmap.fr/fr/map/cartographie-bretagne-transition_124807#12/47.8955/-3.8202)
+ [cartographie.francetierslieux.fr/](https://cartographie.francetierslieux.fr/)
+ [Les acteurs de la transition sur le territoire](https://framacarte.org/fr/map/les-acteurs-des-transitions-sur-le-territoire-de-c_106773#15/47.8665/-3.8991)

## Récolter des données

Un [projet d'arpentage](/articles/arpenter-le-territoire/) va être initier afin d'explorer le paysage pour et grace à la production de données cartographiques et d'analyser le rapport étroit entre activités et paysage (or people, process and places). Les données produites seront standardisées pour permettre leur diffusion et utilisation.

## Voir aussi

+ [virtual-assembly.org/](https://www.virtual-assembly.org/) pour l'approche web sémantique et P2P
