---
layout: page.liquid
date: 2022-04-15
title: Low-tech et sobriété du territoire
tags:
  - acteurs
  - low-tech
  - alimentation
  - mobilite
  - energie
  - habitat

featured: "home"

---

<p class="chapo">La direction régionale de l’ADEME en Bretagne, accompagnée par le Low-Tech Lab, engage 20 structures privées et publiques dans la mise en place de moyens low-tech au service de la sobriété du territoire de Concarneau Cornouaille Agglomération.</p>

<!--more-->

*En écriture*

L'Ademe lance [une consultation](https://www.marches-publics.gouv.fr/app.php/entreprise/consultation/1993085?orgAcronyme=s2d) pour trouver le prestataire qui peut les aider à mener à bien cette expérimentation. Le cahier des charges précise que le projet vise à accompagner ces organisations dans leur transition low-tech pour favoriser la sobriété et la résilience du territoire et pour démontrer à l’échelle d’un territoire que le développement des low-tech est possible.

<blockquote>« Dans un contexte où les ressources naturelles sont limitées, la sobriété consiste à nous questionner sur nos besoins et à les satisfaire en limitant leurs impacts sur l’environnement. Elle doit nous conduire à faire évoluer nos modes de production et de consommation, et plus globalement nos modes de vie, à l’échelle individuelle et collective ».<br>— Ademe</blockquote>

Les objectifs de cette expérimentation sont de :
+ faire fonctionner en système un ensemble de solutions low-tech matures et professionnelles jusque-là isolées,
+ évaluer l’impact du développement de ces solutions, de manière économique, environnementale et de la représentation culturelle associée, au regard d’un territoire démonstrateur,
+ apporter des réponses concrètes aux besoins d’un territoire engagé dans la sobriété,
+ apporter des réponses organisationnelles et/ou techniques.

Il est interessant de pouvoir suivre la trame de ce CDC pour envisager le type ds'actions que doit entreprendre le prestataire choisi. C'est l'objet de cette article.

## Identification des 20 organisations

Pas simple de trouver 20 O. qui n'ont pas déjà sauté le pas des LT. mais qui pourraient être intéressées par cette expérimentation. Une série d'interviews pour faire le choix de ces O. permet aussi de faire un état des lieux de l'envie et de la maturité d'un échantillon d'agents économiques sur les deux points suivants :

1. contribuer à la sobriété du territoire pour faire face à l'urgence climatique,
2. utiliser des moyens low-tech pour contribuer à cette sobriété.

Ces 20 O. doivent couvrir les secteurs d'activités suivants :

+ la mobilité et la logistique
+ le bâtiment et l’hébergement
+ les déchets et leur valorisation
+ l’agriculture, l’alimentation et la restauration

Pour permettre cette phase de recherche, nous pouvons imaginer une typologie d'agents économiques à interroger.

### Des acteurs de la mobilité et de la logistique

Quelques idées à compléter

+ Un transporteur - dernier kilomètre
+ Un réseau de transport en commun
+ ...

### Des acteurs du bâtiment et de l’hébergement

+ Un entrepreneur BTP
+ Un cabinet d'architecture
+ Un distributeur de fioul
+ Un magasin de matériaux
+ ...

### Des acteurs du traitement de déchets et de leur valorisation

+ Une entreprise de démolition
+ Une déchetterie
+ ...

### Des acteurs de l’agriculture, de l’alimentation et de la restauration

+ Un agriculteur
+ Un maraicher
+ Un pécheur
+ Un restaurateur
+ Une conserverie
+ Une entreprise de restauration collective
+ Un restaurant d'école ou lycée
+ ...

### Des acteurs du territoire déjà prescripteurs des low-techs

Il y a sur le territoire des groupes, des associations et des lieux qui ont eux déjà sauté le pas des LT. et qui donc font partie du système qui va accueillir les 20 organisations retenues. Ces acteurs sont force de proposition et doivent être consultés à toutes les étapes de ce projet afin de participer à la réussite de cette expérimentation. Même si certaines de leurs actions sont bénévoles, elles doivent pouvoir être quantifiées dans le cadre de ce projet.

Ces acteurs sont:
+ Atelier Z - où ont lieu les rencontres de Kervic et les Comices du Faire
+ Konk Ar Lab
+ C.R.A.D.E - Concarneau
+ La Konk Creative - Concarneau
+ Alter'Breizh - Quimper
+ Habitat Partagé Tregunc (projet mixant agroforesterie, FabLab rural, vannerie, conserverie) - Tregunc
+ Explore (océans) - Concarneau
+ Zero Waste Cornouaille (...) - Concarneau
+ CCAS Rosporden (mobilités, repair café) -
+ Collectif m.i.t
+ AMAP de Concarneau
+ Librairie Albertine - Concarneau
+ Actes en Cornouailles
+ Kerminy
+ Les Krakous
+ Bretagne Transition
+ La ferme de Kervic
+ à compléter

### Outils et documentation de la phase de Recherche

+ Interviews et captations
+ Matrice 2x2
+ Cartographie
+ Visualisation de données chiffrées
+ Wiki
+ dépendances - cf B. Latour (icono dans dossier Kampus)
+ ...

## Un atelier pour poser les problématiques à résoudre

Après la phase de recherche, de sélection des 20 O. et de documentation de la capacité de sobriété du territoire et de chacune des 20 0., un atelier collaboratif avec l'ensemble des parties prenantes du système "territoire" permet de s'accorder sur les enjeux de l'expérimentation, des facteurs de sobriété et de poser l'ensemble des problématiques à résoudre grâce aux low-Tech.

### Les facteurs de sobriété

Ces facteurs doivent être clairs, partagés et adoptés. Il ne s'agit pas d'adapter une activité économique aux contraintes imposées par l'urgence climatique mais d'[adapter des besoins humains élémentaires](/articles/adapter-les-besoins-au-territoire/) à ce que peut offrir le territoire afin de pouvoir valider l'émergence d'une nouvelle typologie d'agents économiques et des rapports qui en découlent.

### Une problématique à l'échelle du système

Pour assurer l'adhésion des parties prenantes à l'idée qu'elles représentent ensemble un système "territoire", la première problématique à valider est celle à laquelle doit répondre ce système. Pas une problématique à laquelle pourraient répondre chacune des parties prenantes mais une problématique à laquelle peuvent répondre l'ensemble de ces parties prenantes.

Toute la difficulté réside dans la capacité à produire au cours de cet atelier une représentation du système dans lequel les 20 organisations se sentent légitimes et complémentaires et que l'ensemble de ce système soit d'accord sur l'unique question à laquelle répondre.

### Les besoins auxquels les organisations veulent répondre

Il s'agit ensuite d'accompagner la production de problématiques précises à répondre et pour le système "territoire" de valider chacune des 20 problématiques produites. Chacune des O. devra commencer cette expérimentation en ne répondant qu'à une seule problématique. Une approche MVP est indispensable pour permettre un prototypage rapide. Cette première approche peut ensuite être l'objet de plusieurs itérations et produire plusieurs versions du prototype.

Les low-techs ne sont pas une finalité mais un moyen (ou LE moyen dans le cadre précis de cette expérimentation) d'atteindre un objectif qui ne peut qu'être la sobriété du territoire mais qui doit prendre en compte les objectifs de l'O. en tant qu'agent économique.

L'ensemble de ces problématiques est documenté dans le Wiki de l'expérimentation.

## Choix des indicateurs

Un choix d'indicateurs précis va permettre de produire une vue avant/après pour les 3 facteurs suivants :

+ financiers (Coût, économies, aides…)
+ environnementaux (GES évités, ressources économisées…)
+ représentation culturelle (réponse aux besoins, acceptabilité, qualité de travail…),

Des outils d'auto-évaluation vont permettre aux 20 O. de contribuer à nourrir cette productions de données qualitatives et quantitatives, tout au long de cette expérimentation. Mais il est essentiel de produire en amont les problématiques à résoudre avant de pouvoir faire ce choix d'indicateurs qui vont permettre de qualifier le succès de l'expérimentation.

## Des solutions au moyen des low-tech

Un atelier d'idéation permet de trouver un ensemble de solutions en réponse aux problématiques produites et convoquer des moyens low-Tech pour permettre que ces solutions soient validées et initiées comme projet dans chacune des O. sans oublier que l'ensemble de ces solutions doivent répondre à la problématique du système "territoire".

Une fois ces solutions lancées, il faut suivre les structures tout au long de l’expérimentation. De l'aide à la mise en œuvre, itération sur les moyens mis en œuvre

## Mesure d'impact

+ Aide à l’évaluation de l’expérimentation
+ Analyser et restituer l’état des lieux final

## Documentation et partage des apprentissages

Une représentation graphique du territoire, des parties-prenantes et des solutions mises en œuvre sera bienvenue tout au long de l’évolution du projet.

*À suivre.*
