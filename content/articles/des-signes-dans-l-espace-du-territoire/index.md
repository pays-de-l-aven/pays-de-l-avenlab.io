---
layout: page.liquid
date: 2022-04-15

title: Des signes dans l'espace du territoire
tags:
- communication
- donnees
- reseau

featured: "home"

---

<p class="chapo">
Tout le travail de récolte et de numérisation de données ne doit pas masquer le besoin de rendre visible ces données dans l'espace du territoire.
</p>

<!--more-->

## Documentation papier et affichage

Livrets, affiches... Se réapproprier les espaces d'affichage libre et associatif.

+ [Les autres possibles](https://lesautrespossibles.fr/tous-les-numeros/)
+ [Le mur mur des Larris](http://www.formes-vives.org/atelier/?post/Le-Mur-Mur-des-Larris) de Formes Vives. Double avantage d'être signalétique et informatif.

## Affichage libre

Il existe aussi un réseau de panneaux d'affichage libre (<mark>à vérifier et à trier</mark>):

### Concarneau

+ Quai de la croix (devant le suqre des Filets Bleus)
+ Rue Maréchal Foch (devant la maison des associations)
+ Place du Dorlett (Rue des roses)
+ Kerandon (rue du 19 novembre devant la Maison de la Petite Enfance)
+ Quai Carnot (parking CCI face à la Poste)
+ Bourg de Beuzec-Conq (rue de Stang Coulz devant le parking)
+ Rue du Rouz (face à la Poste)
+ Cours Charlemagne (face au collège du Porzou)
+ Bourg de Lanriec (rue de Penhars-Poulyoud le long de l'école)
+ Avenue Pierre Guéguin (Parking d'Aiguillon)
+ Keramporiel (Rue du Cacatois)

### Elliant

+ Parking salle polyvalente

### Melgven

+ Salle polyvalente - Rue Per Jakez Hélias

### Nevez

+ Place de l'église
+ Rue de Kerillis - devant la salle des fêtes et la mairie

### Pont-Aven

+ Mairie – 29 rue Louis Lomenech
+ Gymnase de Penanroz
+ Place des Grands Chênes

### Rosporden
+ Rue du Four
+ Rue de Coray
+ Le Pont Biais
+ Rue de Pont-Aven
+ Parking du Ruveil
+ Place de l'église - Kernével
+ La Croix Lanveur - Kernével
+ Route de Scaër - Kernével
+ Keransquer - Kernével

### Saint-Yvi

+ Place du Général de Gaulle (près du stade)
+ Résidence du Bois de Pleuven

### Tourch

+ Place Guillaume Guéguen - derrière la mairie

### Tregunc

+ Mairie - Place de la mairie
+ Restaurant municipal - Place Beaujean
+ Croissant-Bouillet – arrêt de bus
+ Kersaux - près du supermarché
+ Lambell - Lambell Bas
+ Lambell - Lambell Haut
+ Pouldohan - Carrefour Pouldohan-Pendruc
+ Pouldohan - Porz-an-Halen
+ Saint-Philibert - Parking face à l'église
+ Trévignon - Le Port
+ Trévignon - Village


## Signalétique

+ [Signalétique de l'échangeur de Vitrolles](http://www.formes-vives.org/atelier/?post/signaletique-vitrolles-echangeur)
+ [Las Vegas Crugny](http://www.collectifetc.com/realisation/las-vegas-crugny/) du Collectif Etc
+ [Voyons Voir](http://www.collectifetc.com/realisation/voyons-voir/) du Collectif Etc
+ Le [Parcours Fil Rouge](https://www.mulhouse.fr/medias/Culture-sports-loisirs/tourisme/Visiter-Mulhouse/Parcours-fil-rouge-2015.pdf) à Mulhouse.

### Une signalétique évolutive

Avec le SGAR de la Préfecture de Région Pays-de-la-Loire, [Vraiment Vraiment](https://vraimentvraiment.com/) a conçu des normographes permettant aux agents de modifier les informations autant que nécessaire, tout en restant cohérents avec la charte Marianne.

![Signalétique évolutive avec Vraiment Vraiment](/articles/des-signes-dans-l-espace-du-territoire/vraiment-vraiment-signaletique-pays-de-loire-4.png)


## Voir aussi:

+ [Les objets emblématiques du territoire](/articles/objets-emblematiques-du-territoire)
