---
layout: page.liquid
title: Un schéma directeur mobilité
tags:
  - mobilite
---

Faire le tri entre les moyens et les objectifs

<!--more-->

## Objectifs


Vous trouverez entre parenthèses la provenance des différents objectifs dont les sources sont en lien ci-dessous.
+ A-T2050 - Ademe - [Transition(s) 2050](https://transitions2050.ademe.fr/) - S1, S2, S3 et S4 indiquent si besoin le scénario de référence
+ NW-TETS - Scénario négaWatt 2022 - [La transition énergétique au cœur d’une transition sociétale](https://negawatt.org/Scenario-negaWatt-2022)

---

+ Modération des vitesses de circulation sur les routes avant 2025 (A-T2050-S1)
+ Fin des subventions et des exonérations fiscales existantes sur les énergies fossiles (A-T2050-S1)
+ Évolution des normes et des réglementations sur les véhicules neufs, pour favoriser les véhicules les plus légers et les plus sobres (A-T2050-S1)
+ Évolution de la fiscalité, afin d’internaliser dans le prix des transports les différentes externalités (A-T2050-S2)
+ Création d'écosystèmes de mobilité sobres en énergie et en ressources (A-T2050-S2)
+ Développement d’énergies bas carbone adaptées au contexte local (A-T2050-S2)
+ Lancement d’un plan d’infrastructures efficaces pour les modes sobres (vélo, marche, ferroviaire, fluvial) (A-T2050-S2)
+ Transformation de l’espace public dans les villes et villages au bénéfice de ces modes (A-T2050-S2)
+ Un report important des déplacements en voiture et en avion vers les transports en commun, la marche, le vélo, etc. (NW-TETS)
+ Une diminution des distances parcourues (télétravail, réduction des déplacements très longue distance) (NW-TETS)
+ Le développement du covoiturage et de l’auto-partage (NW-TETS)
+ Une baisse de la vitesse en ville et sur autoroute (NW-TETS)
+ Une réduction de 60 % de la consommation moyenne des voitures (NW-TETS)
+ Investir massivement dans les transports en commun et les infrastructures cyclables, et abandonner tout nouveau projet routier ou aéroportuaire (NW-TETS)
+ Instaurer une redevance kilométrique sur le fret routier afin de financer le fret ferroviaire (NW-TETS)
+ Augmenter le prix de l’aérien (éco-contribution sur les billets d’avion, fiscalité du kérosène, etc.) et interdire progressivement l’ensemble des vols intérieurs lorsqu’une alternative ferroviaire existe (NW-TETS)
+ Interdire la publicité pour l’aérien et les véhicules soumis au malus écologique (NW-TETS)
+ Mettre fin aux ventes de véhicules essence/diesel au plus tard en 2035 (NW-TETS)
+  Promouvoir une réglementation européenne visant à limiter l’impact 6 environnemental des batteries et la consommation de matières premières associées (NW-TETS)
+ Réduire la vitesse maximale autorisée sur autoroute à 110 km/h (NW-TETS)

### et aussi

+ Mise en place d'une taxe carbone
+ Développer un programme OpenData

### Évaluation à 6 ans du schéma de cohérence territoriale  de la circulation

L’organisation de la desserte en transports collectifs sera déclinée en fonction des objectifs figurants ci-dessous :
+ Organiser la desserte des pôles structurants de Concarneau et Rosporden et de leurs zones d’activités, notamment en améliorant la connexion entre ces deux pôles et
en la prolongeant vers Trégunc ;
+ Organiser les déplacements scolaires, de proximité (achats, accès aux services), de loisirs et les déplacements liés au travail ;
+ Optimiser le fonctionnement du réseau « urbain » ;
+ Organiser des liaisons touristiques (lignes des plages) et vers les équipements d’intérêt communautaire (hôpital, piscines...) ;
+ Organiser des liaisons avec les territoires voisins : agglomérations de Quimper et de Quimperlé ;
+ Ces prescriptions sont issues du schéma élaboré à l’échelle du SCoT de Concarneau Cornouaille qui préfigure la future desserte du territoire en transports collectifs.

Si la voiture reste le mode de transport le plus utilisé sur le territoire (et l’ensemble de la Cornouaille), 3 % des actifs occupés utilisent les transports en commun (TC) quotidiennement, ce chiffre est stable entre 2009 et 2014. En 2009 et en 2014, l’on observe qu’aucun actif occupé n’utilise de transports en commun sur la commune de Saint-Yvi. De même sur la commune de Concarneau en 2014 seuls 4% des actifs utilisent les TC contre 3% en 2009.

Dans l’ensemble, les modes de transports alternatifs à la voiture restent peu utilisés. Ceci peut s’expliquer par la dispersion des logements et des lieux de travail ou de loisir qui ne permettent pas une utilisation facile des modes doux (vélo, marche) ou des transports en commun.

Il faut toutefois noter le développement du réseau de transport urbain de CCA (réseau Coralie) qui a pris cette compétence en 2012 et l’a étendu à l’ensemble des neuf communes, notamment avec le transport à la demande. En outre, les orientations du SCoT ont été complétées en 2016 par l’élaboration d’un Plan Global de Déplacements (PGD) comprenant un schéma des mobilités durables et une programmation des itinéraires des modes doux. La mise en œuvre de ce document reste à évaluer.

## Programme d'actions Cit'ergie CCA 2020-2022

+ Définir les modalités de suivi des parts de chaque mode de déplacement (EDVM ou google)
+ Agir sur les changements de comportement liés aux mobilités.
+ Poursuivre les actions de développement de la multimodalité et la promotion du covoiturage.
+ Poursuivre et amplifier les actions de sensibilisation des agents à la mobilité durable.
+ Développer les formations d'écoconduite.
+ Engager une réflexion avec les professionnels de la logistique pour étudier les possibilités d'action.
+ Poursuivre et développer l'accompagnement des communes pour le développement du réseau piétonnier du quotidien.
+ Poursuivre et développer l'accompagnement des communes pour le développement du réseau cyclable du quotidien.
+ Suivre le déploiement des infrastructures cyclables.
+ Aménager le pôle d'échange multimodal de Rosporden.
+ Aménager le pôle d'échange multimodal de Concarneau


## Moyens

+ infrastructures du territoire - multi-modalité


## Questions

+ Comment initier le report modal ?
+ Quelles mesures? Quels indicateurs d'encouragement ?

## Resssources

+ [Et si un indicateur de cyclabilité aidait à objectiver les politiques publiques et à mieux comprendre les dynamiques locales ?](https://www.velo-territoires.org/actualite/2022/05/11/indicateur-de-cyclabilite/)
