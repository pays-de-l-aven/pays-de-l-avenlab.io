---
layout: page.liquid
title: Entretiens et ateliers
tags:
  - donnees
  - references
---

Références à compléter

<!--more-->

## Références

Lire [la représentation de l’adaptation aux changements climatiques entre Montréal et Paris à travers l’analyse lexicométrique](https://www.linkedin.com/pulse/la-repr%C3%A9sentation-de-ladaptation-aux-changements-et-%C3%A0-simonet-phd?trk=public_profile_article_view) de Guillaume Simonet.
