---
layout: page.liquid
title: Auteurs à suivre et à lire
tags:
  - donnees
  - references

---

Liste perso de livres à lire

<!--more-->

+ Vinciane Despret - [cairn.info/publications-de-Vinciane-Despret](https://www.cairn.info/publications-de-Vinciane-Despret--82358.htm)
+ Lovelock et Margulis - *l'hypothèse Gaïa*
+ Donna Haraway -
