---
layout: page.liquid
date: 2021-12-16

title: Les enjeux de Bretagne Transition
tags:
  - cooperation
  - mobilite
  - habitat
  - alimentation
  - energie
  - low-tech

featured: "home"

---

<p class="chapo">Sur <a href="https://bretagne-transition.org/">son site</a>, BT définit son action comme suit. "Afin d’anticiper les nombreux défis à venir de façon positive, Bretagne Transition accompagne les acteurs du territoire en faveur d’une transition citoyenne durable et équitable. Pour cela, l’association apporte des éléments de compréhension de ces enjeux et facilite le partage d’expériences innovantes concrètes ancrées sur les territoires. Elle organise des conférences et ciné-débats avec des professionnels, des balades et visites pour découvrir de nouveaux modes de fonctionnement de la société, des évènements de diffusion et mise à disposition de l'information pour entamer la réflexion..."</p>

<!--more-->

## Le dimensionnement des actions et du territoire

Les actions de BT sont:
- Comprendre les enjeux et problèmes à dépasser
- Imaginer des territoires créateurs de valeurs fortes
- Mobiliser des savoirs et des techniques innovantes
- Expérimenter pour donner l'envie d'agir

Mais la taille du territoire d'expérimentation de BT reste difficile à appréhender - La CCA, l'ancien pays de l'Aven, la Cornouaille, le Finistère, la Bretagne... Quel territoire pour quelles actions?

Le [projet SITI](https://wiki.resilience-territoire.ademe.fr/wiki/SITI), documenté sur le wiki *Résilience des territoires* de l'Ademe, propose de mettre en œuvre une démarche agile et coopérative, mobilisant des solutions logicielles ouvertes et interopérables, pour co-construire des systèmes d’information en biens communs au service de la résilience des territoires. Ce projet identifie 2 territoires d'expérimentation pour l'année 2022: Pays de Bocage dans l'Orne en Normandie et Compiègne dans l'Oise dans les Hauts-de-France. Si BT envisage un territoire de même taille, il pourrait aller de Nevez à Tourc'h et de Concarneau à Moëlan-sur-mer. Ce choix permettrait une éventuelle collaboration avec les équipes SITI de l'Assemblée Virtuelle.

De nombreuses expérimentations sont déjà menées au sein dans ce territoire, un socle solide pour de nouvelles expérimentations et toujours plus de synergie.

## Arpenter le territoire

Les moyens de communication au sein du territoire sont indissociables des actions et expérimentations entreprises et à entreprendre. Comment mener une action d'un point A à un point B? Quelles moyens de communication entre les différents acteurs du territoire? Et comment communiquer en dehors du réseau d'acteurs et favoriser la sensibilisation et l'inclusion des citoyens dans cette dynamique de transition du territoire?

### Une mobilité verte

Le déplacement en voiture reste un des points de vulnérabilité d'un territoire et va à l'encontre de ce besoin de transformation du territoire. D'autres mobilités sont envisagées et doivent être encouragées. Le triptyque marche, vélo, transports collectifs doit être au cœur de cette transition. Le [Low-tech Tour Californouaille](https://lowtechlab.org/fr/actualites-blog/low-tech-tour-2021) réalisé par l’équipe du Low-tech Lab sur le territoire de Concarneau et ses alentours, en juillet dernier est emblématique (voir [la vidéo](https://youtu.be/AW0pX2fj9yI) de l'expédition). Le rôle de la remorque partagée pour répondre à des besoins logistiques est lui aussi très interessant. Ces nouvelles mobilités peuvent donner des clefs sur le dimensionnement du territoire d'expérimentation et offre un nouveau regard sur le paysage.

### Un dialogue avec les acteurs

La mise en place de cette base de données est l'occasion d'un dialogue soutenu avec l'ensemble des acteurs. Un protocole d'interview peut être mise à disposition pour aider ces rencontres. Une captation vidéo peut être réalisée à chaque interview pour contribuer à la communication BT. Ces différents interviews sont le moyen de collecte des données.

### Une base de données partagée

La matérialisation du tissu d'acteurs pourrait se traduire en la mise en place et la gestion d'une base de données partagée qui recense: les individus, les groupes, les événements, les espaces, les projets, les documents, les thématiques, les idées, les ressources et les besoins ([cette ontologie reste à définir](/articles/l-ontologie-du-pays-de-l'aven/)). elle pourrait également identifier les vulnérabilités du territoire, c'est à dire des facteurs qui pourraient mettre en péril les différents acteurs et actions.

### Des moyens de communication efficaces et durables

Les acteurs identifiées dans la base de données pourraient alors bénéficier d'un moyen simple d'interaction ainsi que d'un moyens simple de diffusion de l'information sur l'ensemble des actions et événements proposés.

Ces choix techniques et technologiques doivent permettre l'interopérabilité des données et être indépendants des plateformes et des offres "cloud" des GAFAM ou autres acteurs. Les acteurs du territoire doivent rester en possession de leurs données. Cette infrastructure peut aussi se penser dans un esprit low-tech et en respect avec un numérique plus soutenable.

Dans un premier temps, la mise en place d'un outil comme [element.io](https://element.io/), même s'il s'agit d'une solution "cloud" permettrait, à l'instar de Discord ou de Slack, la création d'espaces de discussion et une première mutualisation des initiatives et de la gestion de différents projets.

### Le besoin d'information et d'inclusion

Une partie des informations et interactions des différents acteurs doit être accessible à l'ensemble des citoyens du territoire en utilisant les médias les plus adaptés. Et l'ensemble des choix techniques restent ouverts pour permettre l'inclusion de toutes les initiatives allant dans le sens des actions de Bretagne Transition.

## 2 projets d'animation du territoire

### Entraide et festivités

Une première expérimentation de mise en place de la base de données des acteurs et d'un moyen d'interaction et de diffusion d'informations peut être mise en place avec un soucis de simplicité sans attente d'infrastructure lourde. "Entraide et festivités" serait limité à 2 fonctionnalités: 1 - un moyen efficace de demande d'aide dans le cadre des actions de transformation du territoire et 2 - une diffusion d'information des moments festifs qui accompagnent cette transformation. Ce projet peut se faire en synergie avec le projet LoRa du Konk Ar Lab. Voir [Freaks, un réseau local résilient](/articles/freaks-un-reseau-resilient-d-information-locale/).

### Une signalétique à l'échelle du territoire

Pour donner un caractère tangible à l'ensemble des acteurs et actions, une signalétique pourrait être mise en place sur le territoire. elle encouragerait une mobilité verte pour la découverte et la participation aux différents actions, lieux et événements qui sont au cœur de cette transformation du territoire.

### Des équipes de conception

Chacun de ces 2 projets demande la mise en place d'une équipe projet pluridisciplinaire (à définir).
