---
layout: page.liquid
title: Identifier le capital naturel critique
tags:
  - acteurs
  - lieux
  - donnees    
---

Quelques ressources

<!--more-->

Jerôme Pelenc défini le CNC selon 4 critères

1. Essentiel au bien-être des générations présentes et futures
2. Strictement non-substituable étant donné sa contribution unique au bien-être
3. Sa perte serait irréversible
4. Sa destruction pose des questions éthiques

&rarr; Lire [Adapter les besoins au territoire](/articles/adapter-les-besoins-au-territoire/)
