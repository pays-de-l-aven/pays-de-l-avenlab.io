---
layout: page.liquid
title: Les acteurs du pays de l'Aven
tags:
  - acteurs
  - lieux
---

À découvrir.

<!--more-->

+ [Kerminy](https://kerminy.org/fr/)
+ [Atelier Z](http://www.atelierz.xyz/) - où ont lieu [les rencontres de Kervic](http://rencontresdekervic.fr/) et les Comices du Faire
+ [Low-Tech Lab](https://lowtechlab.org/fr) - <mark>Faire un point sur le LowTechTour</mark>
+ [Konk Ar Lab](https://www.konkarlab.bzh/)
+ Les Krakous - Melgven
+ Bretagne Transition

## Voir aussi

+ C.R.A.D.E - Concarneau
+ La Konk Creative - Concarneau
+ Alter'Breizh - Quimper
+ Habitat Partagé Tregunc (projet mixant agroforesterie, FabLab rural, vannerie, conserverie) - Tregunc
+ Explore (océans) - Concarneau
+ Zero Waste Cornouaille (...) - Concarneau
+ Association Paresse
+ CCAS Rosporden (mobilités, repair café) -
+ Névez en Transition
+ Collectif m.i.t
+ AMAP de Concarneau
+ Librairie Albertine - Concarneau
+ Actes en Cornouailles


## Consulter
+ [presdecheznous.fr](https://presdecheznous.fr/annuaire#/fiche/Bateau-de-peche-ZEUS-FABER/K96/nevez@47.834,-3.859,13z?cat=all)
+ [Risposte créative](https://ripostecreativebretagne.xyz/)
