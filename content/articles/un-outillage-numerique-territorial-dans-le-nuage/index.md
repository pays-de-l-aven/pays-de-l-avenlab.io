---
layout: page.liquid
date: 2022-10-19
title: Un outillage numérique territorial dans le nuage
tags:
  - numerique
  - acteurs
  - donnees
  - outils

featured: "home"

---

Après une première note d'intention *[Un numérique utile et frugal](/articles/un-numerique-utile-et-frugal/)*, essayons de juger de la pertinence d'une solution *Cloud* - aussi indépendante soit-elle - comme outillage numérique territorial possible. Ce point fait suite aux discussions qui animent Explore, le Konk Ar Lab, le Low-tech Lab, Bretagne Transition et la Coop des Milieux (à compléter).

<!--more-->

## Le besoin d'une solution robuste de collaboration et de documentation

Qu'elles soient impliquées ou non dans l'*Expérimentation de territoire low-tech*, différentes structures ne cessent d'exprimer le besoin d'une solution robuste de collaboration et de documentation. AtelierZ, Kerminy, Kerbouzier et d'autres aimeraient disposer d'un *Mobilizon* pour communiquer sur la programmation de leurs différents lieux et associations. Dafarnaum - initiative pour une filière réemploi sur le territoire - voudrait disposer d'un wiki pour donner à voir son stock de matériaux et objets. Il y a de nombreux exemples.

Il semble pertinent de mutualiser ces différents besoins et d'opter pour une solution *Cloud* à la *[Indie Hosters](https://indiehosters.net/)*, indépendante des grands prédateurs. Reste à savoir maintenant qui peut porter et financer cette initiative. Le Konk Ar Lab, Bretagne Transition ou une nouvelle structure ?

Après discussion avec Maxime Guedj d'*Indie Hosters*, la migration n'étant jamais une partie de plaisir, il faudrait dès aujourd'hui opter pour un nom de domaine qui pourrait porter cette offre logicielle et déterminer le nombre de comptes actifs pour cette solution. Pour indication une solution comme celle-la coûte environ 8€/compte/mois, ce qui pourrait représenter un certain coût si le nombre de comptes actifs se multiplie. Pour financer cette initiative, il est aujourd'hui encore possible de déposer un dossier pour répondre à l'Appel à projets "La Fabrique des Possibles".

## Quelles sont les limites de cette solution *Cloud* ?

Quelles que soient les données échangées sur la plateforme choisie, elles ne sont pas territorialisées. Cette solution est dépendante de datacenters et de réseaux qui échappent à ses utilisateurs. Et trop souvent les utilisateurs délèguent à la solution le besoin de sauvegarde, sauvegarde qui devrait être locale - c'est à dire territorialisée pour les individus, les structures et le territoire.

Malheureusement la solution de serveurs - de type *YunoHost* - hébergés par les différentes structures n'ont brillé ni par leur accessibilité, ni par leur robustesse, ni comme moyen de consolidation des données à l'échelle du territoire. Ces solutions encouragent un entre-soi de chacune des structures et ne contribue pas à un outillage numérique territorial et inclusif.

## Les différents temps de l'outillage numérique territorial et inclusif

Une feuille de route peut permettre la mise à disposition rapide d'une suite logicielle pour le territoire tout en questionnant la pertinence de cette offre pour ce même territoire (encore à améliorer)

1. Une structure - encore à définir - répond à l'AAP "La Fabrique des Possibles" afin de financer à court-terme une réponse *Cloud* au besoin d'un outillage numérique territorial et à plus long-terme, de financer les expérimentations et les efforts de sensibilisation visant à contrer les limites de cette offre.
2. Les différentes structures du territoire (et pas seulement celles qui prennent part à l'*Expérimentation de territoire low-tech*) se voit dotées d'une solution robuste à la *Indie Hosters*. On peut penser lancer ce point avant d'avoir le financement - point précédent.
3. Un effort de sensibilisation permet à tous - structures et hors-structures - d'utiliser au mieux la suite logicielle proposée
4. La mise en projets d'expérimentations permet de trouver des premières réponses à la problématique suivante : <mark>Comment la dynamique numérique locale et durable peut faciliter la satisfaction des besoins les plus élémentaires des citoyens ?</mark> Et cela sans être tributaire d'une solution qui échappe au territoire. (Voir - [Freaks, un réseau résilient d'information locale](/articles/freaks-un-reseau-resilient-d-information-locale/)
)

Dans le cadre de cette structure/projet, il sera possible de penser et de mettre en œuvre une sobriété dimensionnelle, coopérative, d’usage, organisationnelle et matérielle. Pour un peu plus d'information, voir la note *[Un numérique utile et frugal](/articles/un-numerique-utile-et-frugal/)*.
