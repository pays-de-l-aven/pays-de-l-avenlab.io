---
layout: page.liquid
date: 2022-02-15

title: Un schéma directeur alimentation
tags:
  - alimentation
  - logistique
  - schema

featured: "home"

---

<p class="chapo">Pour un objectif global de neutralité carbone en 2050, chaque territoire peut actionner différents leviers dont l'ensemble de ceux qui touchent l'alimentation des résidents. Sur la base des différentes propositions d'actions des secteurs public et privé, cette page recense et propose des choix pour constituer une ébauche de schéma directeur pour le Pays de l'Aven.<p>

<!--more-->

<mark>Faire un point sur une approche plus systémique.</mark>

Vous trouverez entre parenthèses la provenance des différents objectifs dont les sources sont en lien ci-dessous.
+ A-T2050 - Ademe - [Transition(s) 2050](https://transitions2050.ademe.fr/)
+ NW-TETS - Scénario négaWatt 2022 - [La transition énergétique au cœur d’une transition sociétale](https://negawatt.org/Scenario-negaWatt-2022)
+ S-A2050 - Solagro - Afterres2050 - [Un horizon pour l'agriculture et l'alimentation](https://afterres2050.solagro.org/)

## Les leviers possibles liés au secteur de l'alimentation

+ Système de production, transformation, commercialisation et valorisation, pratiques, structuration et gouvernance (Cerema)
+ Intrants agricoles &rarr; Production &rarr; Transformation &rarr; Distribution &rarr; Consommation &rarr;  Déchets (Réseau Partage)

**Voir aussi [les 5 défis du PAT Finistère](https://www.finistere.fr/Le-Conseil-departemental/Le-projet-de-partemental/Le-projet-alimentaire-de-territoire).**

+ Défi 1 : connaître les dynamiques alimentaires (Voir l'[enquête sur les habitudes alimentaires des consommateurs finistériens](https://www.finistere.fr/content/download/41127/739184/file/diapo%20enqu%C3%AAte%20assises%20VF%20pour%20sitecd29.pdf))
+ Défi 2 : un consommateur acteur de ses choix
+ Défi 3 : fédérer une diversité d'acteurs
+ Défi 4 : favoriser une alimentation pour tous, issue du territoire
+ Défi 5 : conforter l’alimentation comme vecteur de lien social

## Objectifs

Il est important de faire la différence entre objectifs et moyens. Les rapports cités, pour atteindre la neutralité carbone, proposent des leviers "alimentation" dont une bonne partie est difficile à appréhender par un résident du territoire dont le comportement peut se limiter à : achat &rarr; consommation &rarr; déjection. Je reprends ici les différents objectifs qui pourrait avoir du sens pour cet individu. (L'objectif final pourrait être que cette individu soit impliqué dans la totalité de la chaîne - Biosphère &rarr; Production &rarr; Transformation &rarr; Distribution &rarr; Consommation &rarr;  Déchets.)

La crise que traverse aujourd'hui le Bio est principalement du à un niveau de production trop élevé pour la demande effective. <mark>Sources</mark>.

### Réduire le gaspillage

+ Un objectif budget
+ Réduire les pertes et gaspillages, sur l’ensemble de la chaîne du champ à l’assiette et en activant l’ensemble des leviers disponibles. En particulier, améliorer la connaissance de la chaîne de production et redonner de la valeur à l’alimentation constituent des leviers importants pour changer les comportements. (A-T2050)
+ Faire un point sur la Transformation - créer une nouvelle filière.
+ les nouveaux glaneurs - Faire un point sur les invendus et la transformation

### Diminuer l’apport calorique et la consommation

+ Un double objectif budget et santé
+ diminuer l’apport calorique global (A-T2050) - chiffres?
+ 1/3 de surconsommation en moins en 2050 (S-A2050)

### Baisse de la consommation de viande

+ Un double objectif budget et santé.
+ Réduction de la quantité de protéines animales (-50% de consommation de viande en 2050) (NW-TETS)(S-A2050)

### Baisse de la consommation de poisson

+ TBD

### Baisse de la consommation de produits laitiers

+ Un double objectif budget et sant

### Végétaliser progressivement l’assiette des Français

+ Objectif budget, santé et sociale

### Rapprocher les consommateurs des producteurs

+ Relocalisation de la production pour rapprocher les producteurs des consommateurs (A-T2050)
+ Améloration de la distribution
+ Diminution des flux logistiques - Import/export (S-A2050)

### Précarité alimentaire

+ TBD

### Assurer le bien-être animal

+ Objectif éthique
+ TBD


## Moyens

+ Élaborer une politique de santé publique en faveur de la transition alimentaire (NW-TETS) - Accompagner les changements de comportement - Santé et bien-être
+ Poursuivre l’évaluation des impacts environnementaux de l’alimentation - Fair participer la population aux mesures et production de données.
+ Faire évoluer les modes de production (A-T2050)(S-A2050)
+ Relocalisation de la production pour améliorer la résilience des systèmes alimentaires (A-T2050)
+ Relocalisation de la production pour ancrer des emplois dans les territoires (A-T2050)
+ Un doublement dès 2030 des élevages en pâturage, et une division par deux des systèmes d’élevage intensifs (NW-TETS)
+ La suppression des importations de soja (NW-TETS)
+ Une mutation des pratiques agricoles, avec un basculement de l’agriculture dite conventionnelle vers l’agriculture biologique, l’agroécologie et la production intégrée (NW-TETS)
+ Mettre en place une stratégie de forte réduction des intrants de synthèse (engrais azotés et produits phytosanitaires)(NW-TETS)
+ Renforcer les aides au maintien et au passage en agriculture biologique
+ Généraliser les paiements pour services environnementaux (NW-TETS)
+ Préservation des surfaces disponibles (S-A2050)
+ Pertes et gaspillages divisées par 2 en 2050 (S-A2050)
+ Une agroécologie généralisée (S-A2050)
+ En 2050, la production végétale s’effectue pour 45% en agriculture biologique et pour 45% en production intégrée (S-A2050)



## Questions

+ Existe-t-il un projet alimentaire territorial (PAT) à la CCA?
+ Quelles mesures? Quels indicateurs de succès?


## À lire

+ [Le point de vue du ministère de l'agriculture et de l'alimenation](https://agriculture.gouv.fr/thematique-generale/alimentation)
+ [Collectif les Pieds dans le Plat](https://www.collectiflespiedsdansleplat.org/)


## Ressources

+ [Expérimentation d'une démarche d’auto-évaluation des projets alimentaires territoriaux - Cerema](https://www.cerema.fr/fr/actualites/experimentation-demarche-auto-evaluation-projets-alimentaires)
+ [Projets Alimentaires Territoriaux - DRAAF Bretagne](https://draaf.bretagne.agriculture.gouv.fr/Projets-Alimentaires-Territoriaux)
+ [Guide de réalisation de diagnostic alimentaire pour les PAT de la région Grand Est - Réseau Partage](https://reseau-partaage.fr/guide-de-realisation-dun-diagnostic-alimentaire-pour-les-pat-de-la-region-grand-est/)
+ [Le projet alimentaire de territoire du Finistère](https://www.finistere.fr/Le-Conseil-departemental/Le-projet-de-partemental/Le-projet-alimentaire-de-territoire)


## À lire ou essayer

+ [agrilocal29.fr/](https://www.agrilocal29.fr/)
